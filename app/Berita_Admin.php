<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Berita_Admin extends Model
{
	use SoftDeletes;

	protected $table = 'berita_admin';
	protected $fillable = ['judul','kategori_berita','month','gambar','caption','deskripsi','total_komentar','views'];
	//
	
	protected $dates = ['deleted_at'];
}
