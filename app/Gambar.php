<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
	protected $table = 'banner_admin';
	protected $fillable = ['gambar','nm_banner'];
    //
}
