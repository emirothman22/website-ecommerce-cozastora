<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gambar_Produk extends Model
{
    use SoftDeletes;
    //
    protected $table = 'produk';
    protected $fillable = ['nm_produk','kategori_produk','deskripsi'
    ,'harga','kuantitas','total_komentar','kota','weight','color','size','suka','gambar','model_gambar'];

    protected $dates = ['deleted_at'];
}
