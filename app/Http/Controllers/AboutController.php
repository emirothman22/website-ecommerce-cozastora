<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inbox;

class AboutController extends Controller
{
	public function index()
	{
	$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
	$data['inboxe'] = \DB::table('inbox')->count();
	$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
	$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
	$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
	return view ('About/about_admin',$data);
	}
    //
}
