<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use App\Keranjang;

class AboutUserController extends Controller
{
    //
    public function index()
    {
        if (Auth::check()) {
            # code...
             $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
             $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
             $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
        } else {
            # code...
            $data['totalsuka'] = Wishlist::count();
            $data['totalkeranjang'] = Keranjang::count();
        }
    return view ('AboutUser/about',$data);
    }
}
