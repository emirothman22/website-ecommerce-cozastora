<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gambar_Produk;

class AdminController extends Controller
{

	public function index()
	{
	$data['produk'] = Gambar_Produk::count();
	$data['banner'] = \DB::table('banner_admin')->get();
	$data['berita'] = \DB::table('berita_admin')->count();
	$data['user'] = \DB::table('users')->count();
	$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
	$data['inboxe'] = \App\Inbox::count();
	$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
	$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
	return view ('admin/admin',$data);
	}

	
   
    //
}
