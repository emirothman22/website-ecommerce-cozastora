<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Wishlist;
use App\Keranjang;
use App\Gambar_Produk;
use App\Province;
use App\City;


class AuthController extends Controller
{
    public function login()
    {
        DB::table('users')->get();
        return view('Pages/login');
    }
    public function register()
    {
        DB::table('users')->get();
        return view('Pages/register');
    }
    public function pengaturan()
    {
         $data['users'] = \DB::table('users')->get();
         $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
         $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
         $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
        return view('Pages/pengaturan',$data);
    }
    // public function updateprofile(Request $request, $id)
    // {
    //     $data['users'] = \DB::table('users')->find($id);
    //     return view('Pages/pengaturan', $data);
    // }
    public function update(Request $request, $id)
    {
        $rule = [
            'name'     => 'required|string',
            'email'    => 'required|string',
            'password' => 'required|string|confirmed',
            'gambar'   => 'file|image|mimes:jpg,png,jpeg,svg|max:2048'
        ];
        $this->validate($request, $rule);


        $users = \App\User::find($id);
        $users->name = $request['name'];
        $users->email = $request['email'];
        $users->password = Hash::make($request['password']);
        $users->showpassword = $request['password'];
        if ($users->gambar != null) {
            $users->gambar = $users->gambar;
        } else {
            $users->gambar= $request->gambar;
        }

        if (auth()->user()->provider === 'default') {
            $users->provider = $request->provider;
        } else {
            $users->provider = auth()->user()->provider;
        }
        
        if($request->file('gambar') == "")
        {
            $users->gambar = $users->gambar;
        }
        else
        {
            $file = $request->file('gambar');
            $fileName = $file->getClientOriginalName();
            $request->file('gambar')->move('uploadgambar',$fileName);
            $users->gambar = $fileName;
        }
        $users->update();

        return redirect('/akunuser');
    }
    
     public function getCities($id)
    {
        $city = City::where('province_id',$id)->pluck('title','city_id');
        return json_encode($city);
    }

    public function alamat()
    {
         $data['users'] = \DB::table('users')->get();
         $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
         $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
         $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
         $data['provinces'] = Province::pluck('title', 'province_id');
         $data['cities'] = City::pluck('title', 'city_id');
        return view('Pages/pengaturan_alamat',$data);
    }
    public function updateAlamat(Request $request, $id)
    {
       $this->validate($request, [
            'kota' => 'required',
            'alamat_lengkap' => 'required',
            'telepon' => 'required',
        ]);

        $user = User::find(Auth::id());
        $user->kota = $request->kota;
        $user->kode_pos = $request->kode_pos;
        $user->alamat_lengkap = $request->alamat_lengkap;
        $user->telepon = $request->telepon;
        $user->save();

        Gambar_Produk::where('id',auth()->user()->name)->update([
            'kota' => $request->kota,
        ]);

        return redirect()->back()->with('success','Alamat Berhasil diubah');

    }


    // socialte function 
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect('/akunuser');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $data = User::create([
                'name'     => $user->name,
                'email'    => !empty($user->email)? $user->email : '' ,
                'password' => !empty($user->password)? $user->password : '' ,
                'provider' => $provider,
                'gambar'   => $user->getAvatar(),
                'provider_id' => $user->id
            ]);
            return $data;
        }
    }
}
