<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use Illuminate\Support\Facades\DB;
use Session;



class BannerController extends Controller
{
    //
    public function index()
    {
   		// Function Eloquent//
		$data['banner'] = Gambar::all();
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();

		return view('banner/banner',$data);
    }

    public function tambah()
    {
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
    	return view('banner/tambah',$data);
    }
    public function store(Request $request)
    {
		//membuat sweet alert
		$this->validate($request, [	
			'gambar'	=> 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048',
		]);
			
		$gambar = $request->file('gambar');
        $namaFile = $gambar->getClientOriginalName();
        $request->file('gambar')->move('uploadgambar', $namaFile);
        $do = new Gambar($request->all());
        $do->gambar = $namaFile;
        $do->save();

		return redirect('/banner')->with(['success' => 'Tambah Data Berhasil']);
    }
     //method fungsi hapus data
	public function hapus($id)
	{
		DB::table('banner_admin')->where('id_gambar',$id)->delete();

		//alihkan halaman ke halaman produk
		return redirect('/banner')->with(['success' => 'Hapus Berhasil']);
	}
	public function cari(Request $request)
	{
		//menangkap data pencarian
		$cari = $request->cari;

		//mengambil data dari table pegawai sesuai pencarian data
		$banner = DB::table('banner_admin')
		->where('gambar','like',"%".$cari."%")
		->paginate();

			//mengirim data produk ke view index
		return view('banner/banner',['banner' => $banner]);
	}

}
