<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Berita_Admin;



class BeritaController extends Controller
{
	public function sampah()
	{
		
		//eloquent function
		$data['berita'] = Berita_Admin::onlyTrashed()->paginate(5);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('Berita/sampah',$data);
	}
	public function viewBerita()
	{
		$data['berita'] = \App\Berita_Admin::orderBy('created_at','desc')->paginate(6);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('Berita/viewberita',$data);
	}
	public function detailBerita(Request $request, $id)
	{
		$data['berita'] = \DB::table('berita_admin')->find($id);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('Berita/detailberita',$data);
	}
	public function tambah()
	{
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['kategoriberita'] = \App\KategoriBerita::all();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();

		//memanggil view tambah
		return view('Berita/tambah',$data);
	}
	public function store(Request $request)
	{
		//insert data ke tabel produk,cara ibu
		$rule = [
			'judul' => 'required|string',
			'kategori_berita' => 'required',
			'caption' => 'required',
			'deskripsi' => 'required|string',
			'gambar'    => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048'
		];
		$this->validate($request, $rule);

		$input = $request->all();
		
		//eloquent insert
		$berita = new \App\Berita_Admin;
		$berita->judul = $input['judul'];
		$berita->kategori_berita = $input['kategori_berita'];
		$berita->month = $input['month'];
		$berita->caption = $input['caption'];
		$berita->deskripsi = $input['deskripsi'];
		$berita->gambar = $input['gambar'];
		// $status = $produk->save();
		$gambar = $request->file('gambar');
        $namaFile = $gambar->getClientOriginalName();
        $request->file('gambar')->move('uploadgambar', $namaFile);
        $do = new Berita_Admin($request->all());
        $do->gambar = $namaFile;
        $do->save();
		
		return redirect('/viewberita')->with(['success' => 'Data Berhasil Ditambahkan']);
	}
    //method fungsi hapus data
	public function hapus($id)
	{
		$berita = Berita_Admin::find($id);
		$berita->delete();

		//alihkan halaman ke halaman produk
		return redirect('/viewberita')->with(['success' => 'Data Berhasil Dihapus']);
	}

	//function restrore
	public function restore($id)
	{
		$berita = Berita_Admin::onlyTrashed()->where('id',$id);
		$berita->restore();

		return redirect('/viewberita/sampah');
	}
	public function deletePermanen($id)
	{
		$berita = Berita_Admin::onlyTrashed()->where('id',$id);
		$berita->forceDelete();

		return redirect('/viewberita/sampah');
	}

	public function returnAll()
	{
		$berita = Berita_Admin::onlyTrashed();
		$berita->restore();

		return redirect('/viewberita/sampah');
	}
	public function deleteAll()
	{
		$berita = Berita_Admin::onlyTrashed();
		$berita->forceDelete();

		return redirect('/viewberita/sampah');
	}


		//membuat fungsi edit
	public function edit(Request $request,$id)
	{
		//cara ibu
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['berita'] = \DB::table('berita_admin')->find($id);
		$data['kategoriberita'] = \DB::table('kategori_berita')->get();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('Berita/tambah', $data);
	}
	public function update(Request $request, $id)
	{
		

		// eloquent function
		$rule = [
			'judul' => 'required|string',
			'kategori_berita' => 'required',
			'caption' => 'required',
			'deskripsi' => 'required|string',
			'gambar'    => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048'
		];

		$this->validate($request, $rule);

		$input = $request->all();

		$berita = \App\Berita_Admin::find($id);
		// $status = $produk->update($input);
		$berita->judul = $input['judul'];
		$berita->kategori_berita = $input['kategori_berita'];
		$berita->month = $input['month'];
		$berita->caption = $input['caption'];
		$berita->deskripsi = $input['deskripsi'];


		if($request->file('gambar') == "")
		{
			$berita->gambar = $berita->gambar;
		}
		else
		{
			$file = $request->file('gambar');
			$fileName = $file->getClientOriginalName();
			$request->file('gambar')->move('uploadgambar',$fileName);
			$berita->gambar = $fileName;
		}
		$berita->update();
		//alihkan halaman ke halaman produk
		return redirect('/viewberita')->with(['success' => 'Data berhasil Diupdate']);
	}
	public function cari(Request $request)
	{
		//menangkap data pencarian
		$cari = $request->cari;

		//mengambil data dari table pegawai sesuai pencarian data
		$berita = DB::table('berita_admin')
		->where('judul','like',"%".$cari."%")
		->paginate();

			//mengirim data produk ke view index
		return view('Berita/berita',['berita' => $berita]);
	}
}
