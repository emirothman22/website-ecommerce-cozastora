<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keranjang;

class CartAdminController extends Controller
{
    public function cartAdmin()
    {
        $data['cart'] = Keranjang::paginate(5);
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
        $data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
        $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
        return view('Cart/keranjang',$data);
    }
    public function cari(Request $request)
    {
        $cari = $request->cari;
        $data['cart'] = Keranjang::where('nm_produk','like',"%".$cari."%")->paginate();
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->count();
        $data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
        $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
        return view('Cart/keranjang',$data);
    }
    //
}
