<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use App\Keranjang;
use App\Gambar_Produk;
use App\Province;
use App\City;
use App\Courier;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use App\Kurir;
use GuzzleHttp\Client;

class CheckoutController extends Controller
{
    //
    public function checkout(Request $request)
    {
        if (Auth::check()) {
            $jalur = $request->kota_asal. '-' .$request->kota_tujuan;
            $total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
            $data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat',$total_berat)->get();
            $data['couriers'] = Courier::pluck('title','code');
            $data['provinces'] = Province::pluck('title','province_id');
            $data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
            $data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
            $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
            $data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
        
        } else {
            $data['cart'] = Keranjang::orderBy('created_at','desc')->get();
		    $data['keranjang'] = Keranjang::get();
		    $data['totalkeranjang'] = Keranjang::count();
        }
        
        return view('DetailOrder/Checkout/checkout',$data);
    }

     public function processCheckout(Request $request, $id){
        if (Keranjang::where('pemilik', auth()->user()->name) === null) {
            return redirect()->back();
        } else {
            $harga = Keranjang::find($id);
            Keranjang::where('pemilik', auth()->user()->name)->update([
                'kuantitas' => $request->kuantitas,
                'total' => $harga->harga_produk * $request->kuantitas,
            ]);

            return redirect('DetailOrder/Checkout/checkout');
        }
    }

    
    //mandapatkan api kota
    public function getCities($id)
    {
        $city = City::where('province_id',$id)->pluck('title','city_id');
        return json_encode($city);
    }

    //mendapatkan biaya rajaongkir
    public function submit(Request $request)
    {
        if (auth()->user()->kota === null) {
			return redirect('/pengaturan/alamat')->with('success','mohon isi Alamat terlebih dahulu');
		} else {
			$rule = [
			 'kurir' => 'required|string',
		];
		$this->validate($request, $rule);
		
		$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
		$keranjang = Keranjang::where('pemilik',auth()->user()->name)->first();

		if (session('cart')) {
			$cart = session()->get('cart');
			$total = 0;

			foreach (session('cart') as $id => $details) {
				$total += $details['berat_produk'];
			}
			 $cost = RajaOngkir::ongkosKirim([
				'origin'      => $cart[1]["kota"],
				'destination' => auth()->user()->kota,
				'weight'      => $total + $total_berat,
				'courier'     => $request->kurir,
			])->get();
			
			$jalur = $cart[1]["kota"]. '-' .auth()->user()->kota;

			if (Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->first()) {
				$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
				$data['couriers'] = Courier::pluck('title','code');
				$data['provinces'] = Province::pluck('title','province_id');
				$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
				$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
				$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
				$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
				$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->get();
				return view('DetailOrder/Checkout/checkout', $data);
			} else {
				for ($x = 0; $x <= count($cost[0]['costs'])-1; $x++) {
                        $kurir = new Kurir();
                        $kurir->jalur = $jalur;
                        $kurir->skt_kurir = $cost[0]['code'];
                        $kurir->pjg_kurir = $cost[0]['name'];
                        $kurir->skt_jenis_layanan = $cost[0]['costs'][$x]['service'];
                        $kurir->pjg_jenis_layanan = $cost[0]['costs'][$x]['description'];
                        $kurir->harga = $cost[0]['costs'][$x]['cost'][0]['value'];
                        $kurir->estimasi = $cost[0]['costs'][$x]['cost'][0]['etd'];
                        $kurir->total_berat = $total + $total_berat;
                        $array[$x] = $kurir->save();
				}
				if ($array) {
					$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
					$data['couriers'] = Courier::pluck('title','code');
					$data['provinces'] = Province::pluck('title','province_id');
					$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
					$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
					$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
					$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
					$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->get();
				} else {
					return redirect()->back()->with('ongkir','gagal memeriksa ongkir');
				}
					
			}	
		}
		else {
			$cost = RajaOngkir::ongkosKirim([
				'origin' => $keranjang->kota,
				'destination' => auth()->user()->kota,
				'weight' => $total_berat,
				'courier' => $request->kurir,
			])->get();


			$jalur = $keranjang->kota. '-' .auth()->user()->kota;
			if (Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total_berat)->first()) {
				$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
				$data['couriers'] = Courier::pluck('title','code');
				$data['provinces'] = Province::pluck('title','province_id');
				$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
				$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
				$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
				$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
				$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total_berat)->get();
				return view('DetailOrder/Checkout/checkout', $data);
			} else {
				for ($x = 0; $x <= count($cost[0]['costs'])-1; $x++) {
                        $kurir = new Kurir();
                        $kurir->jalur = $jalur;
                        $kurir->skt_kurir = $cost[0]['code'];
                        $kurir->pjg_kurir = $cost[0]['name'];
                        $kurir->skt_jenis_layanan = $cost[0]['costs'][$x]['service'];
                        $kurir->pjg_jenis_layanan = $cost[0]['costs'][$x]['description'];
                        $kurir->harga = $cost[0]['costs'][$x]['cost'][0]['value'];
                        $kurir->estimasi = $cost[0]['costs'][$x]['cost'][0]['etd'];
                        $kurir->total_berat = $total_berat;
                        $array[$x] = $kurir->save();
				}
				if ($array) {
					$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
					$data['couriers'] = Courier::pluck('title','code');
					$data['provinces'] = Province::pluck('title','province_id');
					$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
					$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
					$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
					$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
					$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total_berat)->get();
				} else {
					return redirect()->back()->with('ongkir','gagal memeriksa ongkir');
				}
			}
			
		}

	}
		
    }
    
}
