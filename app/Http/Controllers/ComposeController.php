<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomentarBerita;

class ComposeController extends Controller
{
    public function index()
    {
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \DB::table('inbox')->count();
		$data['komentar'] = \DB::table('t_komentar')->count();
        return view('Inbox/compose',$data);
    }
    //
}
