<?php

namespace App\Http\Controllers;
use App\Inbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use App\Keranjang;

class ContactUserController extends Controller
{
	
    //
	public function index()
	{
		if (Auth::check()) {
			# code...
			$data['inbox'] = \DB::table('inbox')->get();
			$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
			$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
			$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		} else {
			# code...
			$data['inbox'] = \DB::table('inbox')->get();
		}
    	return view('ContactUser/contact',$data);
    }
    public function createKomen()
    {	
		if (Auth::check()) {
			# code...
			$data['inbox'] = \DB::table('inbox')->get();
			$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
			$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
			$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		} else {
			# code...
			$data['inbox'] = \DB::table('inbox')->get();
		}
    		return view('ContactUser/contact',$data);
    }
    public function kirimKomentar(Request $request)
    {
    	$rule = [
			'komentar' => 'required|string',
		];
		$this->validate($request, $rule);		
		if (Auth::check()) {
			Inbox::create([
			'email' => auth()->user()->email,
			'komentar' => $request->komentar,
			'gambar'   => auth()->user()->gambar,
			'subjek'   => $request->subjek,
			'status'   => 'unread',
			
		]);
		} else {
			# code...
			Inbox::create([
			'email' => $request->email,
			'komentar' => $request->komentar,
			'gambar'   => $request->gambar,
			'subjek'   => $request->subjek,
			'status'   => 'unread',
			
		]);
		}
		
		
		return redirect('/contact');

    }
}
