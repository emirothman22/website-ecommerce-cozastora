<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use App\Keranjang;
use App\Gambar_Produk;
use App\Province;
use App\City;
use App\Courier;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use App\Kurir;
use GuzzleHttp\Client;
class DetailOrderController extends Controller

{
	public function index(Request $request)
	{
	if (Auth::check()) {
		# code...
		$jalur = $request->kota_asal. '-' .$request->kota_tujuan;
		$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
		$data['kurir'] = Kurir::where('jalur', $jalur)->where('skt_kurir', $request->kurir)->where('total_berat',$total_berat)->get();
		$data['couriers'] = Courier::pluck('title','code');
        $data['provinces'] = Province::pluck('title','province_id');
		$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
		$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
		$data['cek_keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->count();
	
	} else {
		# code...
		$data['couriers'] = Courier::pluck('title','code');
		$data['provinces'] = Province::pluck('title','province_id');
		$data['cart'] = Keranjang::orderBy('created_at','desc')->get();
		$data['keranjang'] = Keranjang::get();
		$data['totalkeranjang'] = Keranjang::count();
	}			
	return view('DetailOrder/order',$data);
	}

	//Checkout 
	//mandapatkan api kota
    public function getCities($id)
    {
        $city = City::where('province_id',$id)->pluck('title','city_id');
        return json_encode($city);
    }

    //mendapatkan biaya rajaongkir
    public function submit(Request $request)
    {
		// if (Auth::check()) {
		// # code...
		// $data['couriers'] = Courier::pluck('title','code');
        // $data['provinces'] = Province::pluck('title','province_id');
		// $data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
		// $data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
		// $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		// $data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');

		if (auth()->user()->kota === null) {
			return redirect('/pengaturan/alamat'. auth()->user()->name)->with('message','mohon isi Alamat terlebih dahulu');
		} else {
			$rule = [
			 'kurir' => 'required|string',
		];
		$this->validate($request, $rule);
		
		$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
		$keranjang = Keranjang::where('pemilik',auth()->user()->name)->first();

		if (session('cart')) {
			$cart = session()->get('cart');
			$total = 0;

			foreach (session('cart') as $id => $info) {
				$total += $info['berat_produk'];
			}
			 $cost = RajaOngkir::ongkosKirim([
				'origin'      => $cart[1]["kota"],
				'destination' => auth()->user()->kota,
				'weight'      => $total + $total_berat,
				'courier'     => $request->kurir,
			])->get();
			
			$jalur = $cart[1]["kota"]. '-' .auth()->user()->kota;

			if (Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->first()) {
				$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
				$data['couriers'] = Courier::pluck('title','code');
				$data['provinces'] = Province::pluck('title','province_id');
				$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
				$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
				$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
				$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
				$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->get();
				return view('DetailOrder/process-checkout', $data);
			} else {
				for ($x = 0; $x <= count($cost[0]['costs'])-1; $x++) {
                        $kurir = new Kurir();
                        $kurir->jalur = $jalur;
                        $kurir->skt_kurir = $cost[0]['code'];
                        $kurir->pjg_kurir = $cost[0]['name'];
                        $kurir->skt_jenis_layanan = $cost[0]['costs'][$x]['service'];
                        $kurir->pjg_jenis_layanan = $cost[0]['costs'][$x]['description'];
                        $kurir->harga = $cost[0]['costs'][$x]['cost'][0]['value'];
                        $kurir->estimasi = $cost[0]['costs'][$x]['cost'][0]['etd'];
                        $kurir->total_berat = $total + $total_berat;
                        $array[$x] = $kurir->save();
				}
				if ($array) {
					$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
					$data['couriers'] = Courier::pluck('title','code');
					$data['provinces'] = Province::pluck('title','province_id');
					$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
					$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
					$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
					$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
					$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->get();
				} else {
					return redirect()->back()->with('ongkir','gagal memeriksa ongkir');
				}
					
			}	
		}
		else {
			$cost = RajaOngkir::ongkosKirim([
				'origin' => $keranjang->kota,
				'destination' => auth()->user()->kota,
				'weight' => $total_barat,
				'courier' => $request->kurir,
			])->get();


			$jalur = $cart[1]["kota"]. '-' .auth()->user()->kota;

			if (Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->first()) {
				$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
				$data['couriers'] = Courier::pluck('title','code');
				$data['provinces'] = Province::pluck('title','province_id');
				$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
				$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
				$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
				$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
				$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->get();
				return view('DetailOrder/process-checkout', $data);
			} else {
				for ($x = 0; $x <= count($cost[0]['costs'])-1; $x++) {
                        $kurir = new Kurir();
                        $kurir->jalur = $jalur;
                        $kurir->skt_kurir = $cost[0]['code'];
                        $kurir->pjg_kurir = $cost[0]['name'];
                        $kurir->skt_jenis_layanan = $cost[0]['costs'][$x]['service'];
                        $kurir->pjg_jenis_layanan = $cost[0]['costs'][$x]['description'];
                        $kurir->harga = $cost[0]['costs'][$x]['cost'][0]['value'];
                        $kurir->estimasi = $cost[0]['costs'][$x]['cost'][0]['etd'];
                        $kurir->total_berat = $total + $total_berat;
                        $array[$x] = $kurir->save();
				}
				if ($array) {
					$total_berat = Keranjang::where('pemilik',auth()->user()->name)->sum('berat_produk');
					$data['couriers'] = Courier::pluck('title','code');
					$data['provinces'] = Province::pluck('title','province_id');
					$data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
					$data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
					$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
					$data['subtotal'] = Keranjang::where('pemilik',auth()->user()->name)->sum('total');
					$data['kurir'] = Kurir::where('jalur',$jalur)->where('skt_kurir',$request->kurir)->where('total_berat', $total + $total_berat)->get();
				} else {
					return redirect()->back()->with('ongkir','gagal memeriksa ongkir');
				}
			}
			
		}

	}
		
	// } else {
	// 	# code...
	// 	$data['couriers'] = Courier::pluck('title','code');
    //     $data['provinces'] = Province::pluck('title','province_id');
	// 	$data['cart'] = Keranjang::orderBy('created_at','desc')->get();
	// 	$data['keranjang'] = Keranjang::get();
	// 	$data['totalkeranjang'] = Keranjang::count();
	// }
		
        // $data['cost'] = RajaOngkir::ongkosKirim([
        //     'origin'      => $request->city_origin,
        //     'destination' => $request->city_destination,
        //     'weight'      => $request->weight,
        //     'courier'     => $request->courier,
        // ])->get();
        
		
		// return view('DetailOrder/process-checkout',$data,$data);
    }

	public function addKeranjang(Request $request,$id)
	{
		$produk = Gambar_Produk::find($id);
		$keranjang = Keranjang::where('pemilik', auth()->user()->name)->where('id_produk',$produk->id)->first();

		if ($keranjang) {
			return redirect()->back()->with('success','Produk ini telah ditambahkan dikeranjang');
			# code...
		} else {
			if ($request->kuantitas != null)  {
				Keranjang::create([
					'pemilik' => auth()->user()->name,
					'id_produk' => $produk->id,
					'nm_produk' => $produk->nm_produk,
					'deskripsi_produk' => $produk->deskripsi,
					'gambar_produk' => $produk->gambar,
					'harga_produk' => $produk->harga,
					'kuantitas' => $request->kuantitas,
					'berat_produk' => $produk->weight * $request->kuantitas,
					'stok_produk' => $produk->kuantitas,
					'kota' => $produk->kota,
					'total'	=> $produk->harga * $request->kuantitas,
				]);
				return redirect()->back()->with('success','Produk berhasil dimasukkan keranjang');
			} else {
				Keranjang::create([
					'pemilik' => auth()->user()->name,
					'id_produk' => $produk->id,
					'nm_produk' => $produk->nm_produk,
					'deskripsi_produk' => $produk->deskripsi,
					'gambar_produk' => $produk->gambar,
					'harga_produk' => $produk->harga,
					'kuantitas' => 1,
					'berat_produk' => $produk->weight * $request->kuantitas,
					'stok_produk' => $produk->kuantitas,
					'kota' => $produk->kota,
					'total'	=> $produk->harga
				]);
				return redirect()->back()->with('success','Produk berhasil dimasukkan keranjang');
				# code...
			}
			
		}
	}

	public function updateKeranjang(Request $request,$id)
	{
		$keranjang = Keranjang::where('pemilik', auth()->user()->name)->find($id);
		$keranjang->kuantitas = $request->kuantitas; 
		// $keranjang->harga_produk = $request->harga_produk;
		$keranjang->total = $keranjang->harga_produk * $request->kuantitas;
		$keranjang->berat_produk = $keranjang->berat_produk * $request->kuantitas;
		$keranjang->save();
			
		return redirect()->back()->with('success','Keranjang Berhasil di Update');

	}
	public function remove(Request $request,$id)
	{
		$keranjang = Keranjang::where('id_produk',$id);
		$keranjang->delete();

		if ($keranjang) {
			# code...
			return redirect()->back()->with('success','Produk Berhasil dihapus');
		} else {
			return redirect()->back();
			# code...
		}
		
	}
	public static function cartExist($string)
	{	
		if (Auth::check()) {

		return \DB::table('keranjang')->where('id_produk', $string)->where('pemilik',auth()->user()->name)->first();
		}			
	}


	public static function cartExistSession($string)
	{
		return \DB::table('keranjang')->where('id', $string)->first();
		
	}

	public function addSession(Request $request, $id)
	{
		$produk = Gambar_Produk::find($id);

		if (!$produk) {
			# code...
			abort(404);
		}

		$cart = session()->get('cart');

		//if cart is empty then this the first product
		if (!$cart) {
			# code...

			$cart = [
				$id => [
					'nm_produk' => $produk->nm_produk,
					'deskripsi_produk' => $produk->deskripsi,
					'kuantitas' => 1,
					'harga_produk' => $produk->harga,
					'gambar_produk' => $produk->gambar,
					'berat_produk' => $produk->weight,
					'stok_produk' => $produk->kuantitas,
					'kota' => $produk->kota,
				]
			];

			session()->put('cart', $cart);

			return redirect()->back()->with('success','Produk berhasil ditambahkan kekeranjang');
		}

		 // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {

            $cart[$id]['kuantitas']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('message', 'Produk berhasil ditambahkan ke keranjang');

        }

        // if item not exist in cart then add to cart with quantity = 1
		$cart[$id] = [
			'nm_produk' => $produk->nm_produk,
			'deskripsi_produk' => $produk->deskripsi,
			'kuantitas' => 1,
			'harga_produk' => $produk->harga,
			'gambar_produk' => $produk->gambar,
			'berat_produk' => $produk->weight,
			'stok_produk' => $produk->kuantitas,
			'kota' => $produk->kota,
		];

		session()->put('cart', $cart);

		return redirect()->back()->with('success','Produk berhasil ditambahkan ke keranjang');

	}

	public function updateSession(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

			$cart[$request->id]["kuantitas"] = $request->quantity;

            session()->put('cart', $cart); 

            session()->flash('message', 'Keranjang telah di update');
        }
    }

	public function removeSession(Request $request)
	{
		if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Produk berhasil dihapus dari keranjang');
        }
	}
    //
}
