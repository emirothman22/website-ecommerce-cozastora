<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Inbox;
use App\KomentarBerita;
use App\Gambar_Produk;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class InboxController extends Controller
{

	public function index(){
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \DB::table('inbox')->count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		
		return view('Inbox/inbox',$data);
	}
	public function hapus($id)	
	{
		DB::table('inbox')->where('id',$id)->delete();
		//alihkan halaman ke halaman produk
		return redirect('/inbox');
	}
	public function read(Request $request, $id)
	{
		$data['inboxku'] = \DB::table('inbox')->find($id);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \DB::table('inbox')->count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();

		if (\DB::table('inbox')->where('id',$id)->first()) {
			# code...
			$inbox = Inbox::find($id);
			$inbox->status = 'readed';
			$inbox->save();
			return view('Inbox/read',$data);
		} else {
			# code...
			return redirect()->back();
		}
		

	}

    //
}
