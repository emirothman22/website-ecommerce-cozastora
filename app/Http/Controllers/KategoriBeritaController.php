<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriBerita;
use Illuminate\Support\Facades\DB;
use Session;

class KategoriBeritaController extends Controller
{
	public function index()
	{
	$data['kategoriberita'] = \DB::table("kategori_berita")->get();
	$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
	$data['inboxe'] = \DB::table('inbox')->count();
	$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
	$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
	$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
	return view('KategoriBerita/kategoriberita',$data);
    //
	}
	public function tambah(){
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('KategoriBerita/tambah',$data);
	}
	public function proses(Request $request)
	{
		$input = $request->all();
		unset($input['_token']);
		$status = \DB::table('kategori_berita')->insert($input);

		return redirect('/kategoriberita')->with(['success' => 'Data berhasil Ditambahkan']);
    
	}
	public function edit(Request $request, $id)
	{	
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['kategoriberita'] = \DB::table("kategori_berita")->find($id);
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();

		return view('KategoriBerita/tambah',$data);
	}
	public function update(Request $request, $id)
	{
		$rule = [
			'jenis' => 'required|string',
		];

		$this->validate($request, $rule);
		$input = $request->all();
		$kategoriberita = \App\KategoriBerita::find($id);
		$kategoriberita->jenis = $input['jenis'];
		$kategoriberita->update();

		return redirect('/kategoriberita')->with(['success' => 'Data berhasil Diupdate']);

	}
	public function hapus($id)
	{
		DB::table('kategori_berita')->where('id',$id)->delete();
		return redirect('/kategoriberita')->with(['success' => 'Data Berhasil Dihapus']);
	}
	public function cari(Request $request)
	{
	//menangkap data pencarian
		$cari = $request->cari;

		//mengambil data dari table pegawai sesuai pencarian data
		$kategoriberita = DB::table('kategori_berita')
		->where('jenis','like',"%".$cari."%")
		->paginate();
		$kategoriberita = DB::table('inbox')
		->where('jenis','like',"%".$cari."%")
		->paginate();
		$kategoriberita = DB::table('t_komentar')
		->where('jenis','like',"%".$cari."%")
		->paginate();
		

			//mengirim data produk ke view index
		return view('KategoriBerita/kategoriberita',['kategoriberita' => $kategoriberita]);
	}

}
