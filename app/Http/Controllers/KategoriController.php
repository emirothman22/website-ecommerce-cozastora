<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\KategoriProduk;

class KategoriController extends Controller
{

	public function index(){
		$data['kategoriproduk'] =  \App\KategoriProduk::paginate(5);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
        $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('KategoriProduk/kategori',$data);
	}
	public function tambah(){
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('KategoriProduk/tambah',$data);
	}
	public function store(Request $request)
	{
		//inseet data ke tabel produk,cara ibu
		// $input = $request->all();
		// unset($input['_token']);
		// $status = \DB::table('kategori_produk')->insert($input);

		$rule = [
			'jenis' => 'required|string',
			'gambar'    => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048'
		];
		$this->validate($request, $rule);

		$input = $request->all();

		//eloquent insert
		$kategoriproduk = new \App\KategoriProduk;
		$kategoriproduk->jenis = $input['jenis'];
		$kategoriproduk->size = $input['size'];
		$kategoriproduk->color = $input['color'];
		$kategoriproduk->gambar = $input['gambar'];
		$gambar = $request->file('gambar');
        $namaFile = $gambar->getClientOriginalName();
        $request->file('gambar')->move('kategorigambar', $namaFile);
        $do = new KategoriProduk($request->all());
        $do->gambar = $namaFile;
        $do->save();

		return redirect('/kategori')->with(['success' => 'Data Berhasil Ditambahkan']);
    //
	}
	public function edit(Request $request, $id)
	{
		$data['kategoriproduk'] = \DB::table('kategori_produk')->find($id);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		 $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('KategoriProduk/tambah',$data);

	}
	public function update(Request $request, $id)
	{
		$rule = [
			'jenis' => 'required|string',
		];

		$this->validate($request, $rule);
		$input = $request->all();
		$kategoriproduk = \App\KategoriProduk::find($id);
		$kategoriproduk->jenis = $input['jenis'];
		$kategoriproduk->size = $input['size'];
		$kategoriproduk->color = $input['color'];
		if($request->file('gambar') == "")
		{
			$kategoriproduk->gambar = $kategoriproduk->gambar;
		}
		else
		{
			$file = $request->file('gambar');
			$fileName = $file->getClientOriginalName();
			$request->file('gambar')->move('kategorigambar',$fileName);
			$kategoriproduk->gambar = $fileName;
		}
		$kategoriproduk->update();

		return redirect('/kategori')->with(['success' => 'Data berhasil Diupdate']);
	}

public function hapus($id)
	{
		DB::table('kategori_produk')->where('id',$id)->delete();

		//alihkan halaman ke halaman produk
		return redirect('/kategori')->with(['success' => "Data Berhasil Dihapus"]);
	}
	public function cari(Request $request)
	{
		//menangkap data pencarian
		$cari = $request->cari;

		//mengambil data dari table pegawai sesuai pencarian data
		$kategoriproduk = DB::table('kategori_produk')
		->where('jenis','like',"%".$cari."%")
		->paginate();

			//mengirim data produk ke view index
		return view('KategoriProduk/kategori',['kategoriproduk' => $kategoriproduk]);
	}


}
