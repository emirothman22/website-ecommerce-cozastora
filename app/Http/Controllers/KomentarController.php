<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita_Admin;
use App\KomentarBerita;
use App\Gambar_Produk;
use App\Notifikasi;
use App\Rating;

class KomentarController extends Controller
{
	//function for comment news
  public function komentarBerita(Request $request, $id)
	{

		$rule = [
			'komentar' => 'required|string',
			'name'	   => 'required|string',

		];

		$this->validate($request, $rule);
		$berita = Berita_Admin::find($id);
		$komentar = KomentarBerita::create([
			'komentar' => $request->komentar,
			'kategori' => 'berita',
			'gambar' => auth()->user()->gambar,
			'objek' => $berita->gambar,
			'to_post'  => $berita->id,
			'provider' => auth()->user()->provider,
			'name' 	   => auth()->user()->name,
			'email'    => auth()->user()->email,
			'website'  => $request->website,
		]);

		Notifikasi::create([
				'id_berita' => $berita->id,
				'gambar'   => auth()->user()->gambar,
				'objek'   => $komentar->objek,
				'subjek' => $berita->judul,
				'kategori_notifikasi' => 'berita',
				'status' => 'unread',
				'provider' => auth()->user()->provider,
				'komentar' => $komentar->komentar,
			]);

			
		$count = $berita->total_komentar;
		$berita->total_komentar = $count + 1;
		$berita->save();

		return redirect()->back()->with('success','Komentar Sudah Ditambahkan');
	}
	//function for comment product
	public function komentarProduk(Request $request,$id)
	{
		$rule = [
			'komentar' => 'required|string',
		];

		$this->validate($request, $rule);
		$produk = Gambar_Produk::find($id);

		
		$komentar = KomentarBerita::create([
			'komentar' => $request->komentar,
			'kategori' => 'produk',
			'to_post'  => $produk->id,
			'provider' => auth()->user()->provider,
			'name' 	   => auth()->user()->name,
			'email'    => auth()->user()->email,
			'gambar'   => auth()->user()->gambar,
			'objek' => $produk->gambar,
			'rating'   => $request->rating,
			'delete_status' => 'no',
		]);
	
			Notifikasi::create([
				'id_produk' => $produk->id,
				'gambar'   => auth()->user()->gambar,
				'objek' => $komentar->objek,
				'subjek' => $produk->nm_produk,
				'kategori_notifikasi' => 'produk',
				'status' => 'unread',
				'provider' => auth()->user()->provider,
				'komentar' => $komentar->komentar,
			]);

			Rating::create([
				'id_produk' => $produk->id,
				'id_komentar' => $komentar->id,
				'pemilik' => auth()->user()->name,
				'gambar' => $produk->gambar,
				'nama_produk' => $produk->nm_produk,
				'rating' => $komentar->rating,
			]);
		
		$ratings = $produk->rating;
		$produk->rating = $ratings + $komentar->rating;
		
		$count = $produk->total_komentar;
		$produk->total_komentar = $count + 1;
		$produk->save();
		return redirect()->back()->with('success','Review Sudah ditambahkan');
	}
	public function deleteProduk($id)
	{
		
		$status = KomentarBerita::where('id', $id)
		->where('kategori','produk')->where('name', auth()->user()->name)
		->update(['delete_status' => 'yes',	
		]);
		
		if ($status) {
			return redirect()->back()->with('success','Berhasil Menghapus Komentar');
			# code...
		} else {
			# code...
			return redirect()->back();
		}
		
	}
    //
}
