<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita_Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\KomentarBerita;
use App\Wishlist;
use App\Keranjang;

class NewsUserController extends Controller
{
	
	public function index(){
		if (Auth::check()) {
			# code...
		$data['berita'] = Berita_Admin::
		orderBy('created_at','desc')
		->paginate(3);
		$data['produk'] = \DB::table('produk')->get();
		$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		} else {
			# code...
		$data['berita'] = Berita_Admin::
		orderBy('created_at','desc')
		->paginate(3);
		$data['produk'] = \DB::table('produk')->get();
		$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		}
		
		
		return view('NewsUser/news',$data,$data2);
	}
	public function index2(Request $request,$id)
	{
		if (Auth::check()) {
			# code...
		$berita = Berita_Admin::find($id);
		$data['berita'] = Berita_Admin::find($id);
		$data['produk'] = \DB::table('produk')->get();
		$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		$data['komentar'] = KomentarBerita::where('kategori', 'berita')->where('to_post', $berita->id)->get();
		$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		
		if($berita->views += 1)
		{
			$berita->views == null;
		}
		else{
			$berita->views += 0;
		}
	
		$berita->save();
		} else {
			# code...
		$berita = Berita_Admin::find($id);
		$data['berita'] = Berita_Admin::find($id);
		$data['produk'] = \DB::table('produk')->get();
		$data['komentar'] = KomentarBerita::where('kategori', 'berita')->where('to_post', $berita->id)->get();
		$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		}
		
		return view('NewsUser/detailnews',$data,$data2);
	}
	public function filterdata(Request $request)
    {
		if (Auth::check()) {
			# code...
			$kategoriberita = $request->jenis;
			$data['berita'] = Berita_Admin::where('kategori_berita', '=' ,$kategoriberita)->paginate(3);
			$data['produk'] = \DB::table('produk')->get();
			$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
			$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
			$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
			$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		} else {
			# code...
			$kategoriberita = $request->jenis;
			$data['berita'] = Berita_Admin::where('kategori_berita', '=' ,$kategoriberita)->paginate(3);
			$data['produk'] = \DB::table('produk')->get();
			$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		}
		
     
      return view('NewsUser/news', $data,$data2);
    }
    public function filterdata2(Request $request)
    {
	  $kategoriberita = $request->jenis;
	  $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
      $data['berita'] = Berita_Admin::where('kategori_berita', '=' ,$kategoriberita)->get();
	  $data['produk'] = \DB::table('produk')->get();
	  $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
	  $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->count();
      $data2['kategoriberita'] = \DB::table('kategori_berita')->get();
      return view('NewsUser/detailnews', $data,$data2);
	}

	public function cari(Request $request)
	{
		//menangkap data pencarian
		$cari = $request->cari;
		//mengambil data dari table pegawai sesuai pencarian data
		if (Auth::check()) {
			# code...
		$data['berita'] = Berita_Admin::where('judul','like',"%".$cari."%")->orwhere('kategori_berita','like',"%".$cari."%")
		->paginate();
		$data['produk'] = \DB::table('produk')->get();
		$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->count();
		$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		} else {
			# code...
		$data['berita'] = Berita_Admin::where('judul','like',"%".$cari."%")->orwhere('kategori_berita','like',"%".$cari."%")
		->paginate();
		$data['produk'] = \DB::table('produk')->get();	
		$data2['kategoriberita'] = \DB::table('kategori_berita')->get();
		}
		//mengirim data produk ke view index
		return view('NewsUser/news',$data,$data2);
	}
}
