<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomentarBerita;
use App\Notifikasi;

class NotifikasiController extends Controller
{
    public function index()
    {
        $data['notifikasi'] = Notifikasi::orderBy('created_at','desc')->paginate(5);
        $data['user'] = \DB::table('users')->orderBy('created_at','desc')->paginate(5);
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
        return view('Notifikasi/notifikasi',$data);
    }

    public function detailNotifikasi(Request $request, $id)
    {
        $data['notifikasi'] = Notifikasi::find($id);
        $data['user'] = \DB::table('users')->orderBy('created_at','desc')->paginate(5);
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
        $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
        
        if (\DB::table('notifikasi')->where('id',$id)->first()) {
            $notifikasi = Notifikasi::find($id);
            $notifikasi->status = 'readed';
            $notifikasi->save();
            return view('Notifikasi/detailnotifikasi',$data);
        } else {
            # code...
            return redirect()->back();
        }
        
        
    }

    public function deleteNotifikasi($id)
    {
        Notifikasi::where('id',$id)->delete();
        return redirect()->back()->with('success','Notifikasi Telah Terhapus');
    }

    public static function statusExist($string)
    {
       
        return \DB::table('notifikasi')->where('id', $string)->where('status','readed')->first();
    }
    
    //
}
