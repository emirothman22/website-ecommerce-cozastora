<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\Inbox;

class PesananController extends Controller
{
    public function pesanan()
    {
        $data['pesanan'] = Pesanan::orderBy('created_at','desc')->paginate(5);
        $data['inbox'] = Inbox::orderBy('created_at','desc')->get();
		$data['inboxe'] = Inbox::count();
        $data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
        $data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
        $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
        return view('Pesanan/pesanan',$data);
    }
    public function cari(Request $request)
    {
        //menangkap data pencarian
        $cari = $request->cari;

        //mengambil data dari table pegawai sesuai pencarian data
        $data['pesanan'] = Pesanan::where('pemilik','like',"%".$cari."%")->orwhere('nama_produk','like',"%".$cari."%")
        ->paginate();
            $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
            $data['inboxe'] = \App\Inbox::count();
            $data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
            $data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
            $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();

        //mengirim data produk ke view index
        return view('Pesanan/pesanan',$data);
    }
    //
}
