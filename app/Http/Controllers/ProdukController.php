<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gambar_Produk;
use App\KategoriProduk;
use App\User;

class ProdukController extends Controller
{

	public function index()
	{
		//eloquet function 
		$data['produk'] = \App\Gambar_Produk::orderBy('created_at','desc')->paginate(5);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		return view('Produk/produk', $data);
	}
	public function lihatProduk()
	{
		$data['produk'] = \App\Gambar_Produk::get();
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		return view('Produk/eproduk',$data);
	}
	public function detailProduk(Request $request, $id)
	{
		$data['produk'] = \DB::table('produk')->find($id);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		return view('Produk/eproduk',$data);
	}
	public function tambah()
	{
		$data['kategoriproduk'] =\App\KategoriProduk::all();
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();

		//memanggil view tambah
		return view('Produk/tambah',$data);
	}
	public function store(Request $request)
	{
		//insert data ke tabel produk,cara ibu
		$rule = [
			'nm_produk' => 'required|string',
			'kategori_produk' => 'required',
			'deskripsi' => 'required|string',
			'harga'     => 'required|numeric',
			'kuantitas' => 'required|integer',
			'model_gambar' => 'required|string',
			'gambar'    => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048'
		];
		$this->validate($request, $rule);

		$input = $request->all();
		$price = $request->harga;
		$formatprice = str_replace('.', '', $price);
		$numericprice = (int) $formatprice;

		//eloquent insert
		$produk = new \App\Gambar_Produk;
		$produk->nm_produk = $input['nm_produk'];
		$produk->kategori_produk = $input['kategori_produk'];
		$produk->deskripsi = $input['deskripsi'];
		$produk->harga = $numericprice;
		$produk->kuantitas = $input['kuantitas'];
		$produk->weight = $input['weight'];
		$produk->size = $input['size'];
		$produk->color = $input['color'];
		$produk->kota = '22';
		$produk->model_gambar = $input['model_gambar'];
		$produk->gambar = $input['gambar'];
		$gambar = $request->file('gambar');
        $namaFile = $gambar->getClientOriginalName();
        $request->file('gambar')->move('uploadgambar', $namaFile);
        $do = new Gambar_Produk($request->all());
        $do->gambar = $namaFile;
        // dd($produk);
        $do->save();
	
		return redirect('/produk')->with(['success' => 'Data berhasil diTambahkan']);
	}

	//method fungsi hapus data
	public function hapus($id)
	{
		$produk = Gambar_Produk::find($id);
		$produk->delete();

		//alihkan halaman ke halaman produk
		return redirect('/produk')->with(['success' => 'Hapus Data berhasil']);
	}
	//function trash
	public function trash()
	{
		//mengambil data guru yang sudah dihapus
		$data['produk'] = Gambar_Produk::onlyTrashed()->paginate(5);
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('Produk/trash',$data);
	}
	//method kembali/restore
	public function kembalikan($id)
	{
		$produk = Gambar_Produk::onlyTrashed()->where('id',$id);
		$produk->restore();
		return redirect('/produk/trash');
	}
	//method restore all
	public function kembalikan_semua()
	{
		$produk = Gambar_Produk::onlyTrashed();
		$produk->restore();
		return redirect('/produk/trash');
	}
	//method delete permanen
	public function hapus_permanen($id)
	{
		$produk = Gambar_Produk::onlyTrashed()->where('id',$id);
		$produk->forceDelete();
		return redirect('/produk/trash');
	}
	//method delete all product
	public function allDelete()
	{
		$produk = Gambar_Produk::onlyTrashed();
		$produk->forceDelete();
		return redirect('/produk/trash');
	}

	//membuat fungsi edit
	public function edit(Request $request, $id)
	{

		$data['produk'] = \DB::table('produk')->find($id);
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
		return view('Produk/tambah', $data, $data2);

	}
	public function update(Request $request, $id)
	{
		$rule = [
			'nm_produk' => 'required|string',
			'kategori_produk' => 'required',
			'deskripsi' => 'required|string',
			'harga'     => 'required|numeric',
			'kuantitas' => 'required|integer',
			'model_gambar' => 'required|string',
			'gambar'    => 'required|file|image|mimes:jpg,png,jpeg,svg|max:2048'
		];

		$this->validate($request, $rule);

		$input = $request->all();
		
		$user = User::find($id);
		$produk = \App\Gambar_Produk::find($id);
		$produk->nm_produk = $input['nm_produk'];
		$produk->kategori_produk = $input['kategori_produk'];
		$produk->deskripsi = $input['deskripsi'];
		$produk->harga = $input['harga'];
		$produk->kuantitas = $input['kuantitas'];
		$produk->weight = $input['weight'];
		$produk->size = $input['size'];
		$produk->color = $input['color'];
		$produk->kota = '22';
 		$produk->model_gambar = $input['model_gambar'];

		if($request->file('gambar') == "")
		{
			$produk->gambar = $produk->gambar;
		}
		else
		{
			$file = $request->file('gambar');
			$fileName = $file->getClientOriginalName();
			$request->file('gambar')->move('uploadgambar',$fileName);
			$produk->gambar = $fileName;
		}
		$produk->update();

		return redirect('/produk')->with(['success' => 'Data Berhasil DiEdit']);
	}
	//membuat fungsi pencarian
	public function cari(Request $request)
	{
		//menangkap data pencarian
		$cari = $request->cari;
		//mengambil data dari table pegawai sesuai pencarian data
		$data['produk'] = DB::table('produk')
		->where('nm_produk','like',"%".$cari."%")->orwhere('kategori_produk','like',"%".$cari."%")
		->orwhere('deskripsi','like',"%".$cari."%")
		->paginate();
		$data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		//mengirim data produk ke view index
		return view('Produk/produk',$data);
	}
	
}
