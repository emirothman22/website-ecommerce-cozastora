<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Gambar_Produk;
use App\KomentarBerita;
use App\Wishlist;
use App\Keranjang;

class TokoController extends Controller
{

	public function index()
	{
		if (Auth::check()) {
			# code...
		$data['produk'] = Gambar_Produk::orderBy('created_at','desc')->paginate(10);
		$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		$data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
		} else {
			# code...
		$data['produk'] = Gambar_Produk::orderBy('created_at','desc')->paginate(10);
		$data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
		}
		return view('Toko/product',$data,$data2);
	}
	public function index2(Request $request,$id)
	{
		if (Auth::check()) {
			# code...
		$produk = Gambar_Produk::find($id);
		$data['produk'] = \DB::table('produk')->find($id);
		$data['kategoriproduk'] = \DB::table('kategori_produk')->get();
		$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
		$data['produks'] = \DB::table('produk')->get();
		$data['komentar'] = KomentarBerita::where('kategori','=','produk')->where('to_post','=',$produk->id)->where('delete_status','no')->orderBy('created_at','desc')->get(); 
		$data['totalkomentar'] = KomentarBerita::where('kategori','=','produk')->where('to_post','=',$produk->id)->where('delete_status','no')->count(); 
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		$produk->save();
		} else {
		# code...
		$produk = Gambar_Produk::find($id);
		$data['produk'] = \DB::table('produk')->find($id);
		$data['kategoriproduk'] = \DB::table('kategori_produk')->get();
		$data['produks'] = \DB::table('produk')->get();
		$data['komentar'] = KomentarBerita::where('kategori','=','produk')->where('to_post','=',$produk->id)->where('delete_status','no')->get(); 
		$data['totalkomentar'] = KomentarBerita::where('kategori','=','produk')->where('to_post','=',$produk->id)->where('delete_status','no')->count(); 
		$produk->save();
		}
		
		return view('Toko/productdetail',$data);
	}
	 public function filterData(Request $request)
    {
		if (Auth::check()) {
			# code...
		$kategoriproduk = $request->jenis;
		$data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
		$data['produk'] = Gambar_Produk::where('kategori_produk', '=' ,$kategoriproduk)->paginate(3);
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas'); 
		$data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
		} else {
		# code...
		$kategoriproduk = $request->jenis;
		$data['produk'] = Gambar_Produk::where('kategori_produk', '=' ,$kategoriproduk)->paginate(3);
		$data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
		}
		
      
      return view('Toko/product', $data,$data2);
    }
    public function cari(Request $request)
    {
    	//menangkap data pencarian
		$cari = $request->cari;
		//mengambil data dari table pegawai sesuai pencarian data
		$data['produk'] = Gambar_Produk::where('nm_produk','like',"%".$cari."%")->orwhere('kategori_produk','like',"%".$cari."%")
		->orwhere('deskripsi','like',"%".$cari."%")
		->paginate();
		$data['produk'] = Gambar_Produk::orderBy('created_at','desc')->paginate(10);
		$data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
		$data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
		$data2['kategoriproduk'] = \DB::table('kategori_produk')->get();

			//mengirim data produk ke view index
		return view('Toko/product',$data,$data2);
	}
	//membuat fungsi upload file
    
// 	
    //
}
