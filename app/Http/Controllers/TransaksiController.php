<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\Transaksi;
use App\Kurir;
use App\Keranjang;
use App\Gambar_Produk;
use App\Wishlist;

class TransaksiController extends Controller
{
    public function transaksi()
    {
        $max = Pesanan::where('pemilik',auth()->user()->name)->max('akumulasi_pembelian');
        if (Pesanan::where('pemilik',auth()->user()->name)->where('akumulasi_pembelian',$max)->first() === null) {
            return redirect()->back();
        } else {
            $data['transaksi'] = Transaksi::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->first();
            $data['pesanan'] = Pesanan::where('pemilik',auth()->user()->name)->where('akumulasi_pembelian',$max)->get();
            $data['keranjang'] = Keranjang::orderBy('created_at','desc')->where('pemilik', auth()->user()->name)->get();
            $data['totalkeranjang'] = Keranjang::where('pemilik', auth()->user()->name)->sum('kuantitas');
            $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
            $data['subtotal'] = Pesanan::where('pemilik',auth()->user()->name)->sum('total_harga');
            $data['total'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
            $data['cek_keranjang'] = Keranjang::where('pemilik', auth()->user()->name)->count();
        }    
         return view('DetailOrder/Transaction/transaction',$data);
    }

    public function pilihOngkir($id)
    {
        $kurir = Kurir::find($id);
        $data = collect([$kurir->skt_kurir, $kurir->skt_jenis_layanan, $kurir->harga]);
        session()->put('ongkir',$data);
        return redirect()->back();
    }
    public function pesan(Request $request)
    {
        $max_ps = Pesanan::max('no_resi');
        $increment = $max_ps + 1;
        
        Transaksi::create([
            'pemilik' => auth()->user()->name,
            'kurir' => $request->kurir,
            'jenis_layanan' => $request->jenis_layanan,
            'ongkir' => $request->ongkir,
            'total_harga' => $request->total,
            'metode_pembayaran' => 'cod',
            'status_pembayaran' => 'belum',
            'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),

        ]);
        $keranjang = Keranjang::where('pemilik', auth()->user()->name)->get();
        $ed = Pesanan::where('pemilik',auth()->user()->name)->max('akumulasi_pembelian');

        if (session('cart')) {
            foreach (session('cart') as $id => $details) {
                Pesanan::create([
                    'pemilik' => auth()->user()->name,
                    'id_produk' => $details['id_produk'],
                    'nama_produk' => $details['nama_produk'],
                    'gambar_produk' => $details['gambar_produk'],
                    'deskripsi_produk' => $details['deskripsi_produk'],
                    'harga_produk' => $details['harga_produk'],
                    'berat_produk' => $details['berat_produk'],
                    'kuantitas' => $details['kuantitas'],
                    'total_harga' => $details['harga_produk'] * $details['kuantitas'],
                    'akumulasi_pembelian' => $ed + 1,
                    'status_pengiriman' => 'belum',
                    'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                    'rating' => 'belum', 
                ]);
                $pr = Produk::where('id',$details['id_produk'])->first();
                $pr->kuantitas = $pr->kuantitas - $details['kuantitas'];
                $pr->save();

            }
            foreach ($keranjang as $row ) {
                Pesanan::create([
                    'pemilik' => $row->pemilik,
                    'id_produk' => $row->id_produk,
                    'nama_produk' => $row->nama_produk,
                    'gambar_produk' => $row->gambar_produk,
                    'deskripsi_produk' => $row->deskripsi_produk,
                    'harga_produk' => $row->harga_produk,
                    'berat_produk' => $row->berat_produk,
                    'kuantitas' => $row->kuantitas,
                    'total_harga' => $row->total_harga,
                    'akumulasi_pembelian' => $ed + 1,
                    'status_pengiriman' => 'belum',
                    'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                    'rating' => 'belum',

                ]);
                $pr = Gambar_Produk::where('id',$row->id_produk)->first();
                $pr->kuantitas = $pr->kuantitas - $row->kuantitas;
                $pr->save();
            }
            session()->forget(['cart', 'ongkir']);
        } else {
            foreach ($keranjang as $row) {
                Pesanan::create([
                    'pemilik' => $row->pemilik,
                    'id_produk' => $row->id_produk,
                    'nama_produk' => $row->nm_produk,
                    'gambar_produk' => $row->gambar_produk,
                    'deskripsi_produk' => $row->deskripsi_produk,
                    'harga_produk' => $row->harga_produk,
                    'berat_produk' => $row->berat_produk,
                    'kuantitas' => $row->kuantitas,
                    'total_harga' => $row->total,
                    'akumulasi_pembelian' => $ed + 1,
                    'status_pengiriman' => 'belum',
                    'no_resi' => str_pad($increment,7,0, STR_PAD_LEFT),
                    'rating' => 'belum',

                ]);
                $pr = Gambar_Produk::where('id',$row->id_produk)->first();
                $pr->kuantitas = $pr->kuantitas - $row->kuantitas;
                $pr->save();
            }
        }

        Keranjang::where('pemilik',auth()->user()->name)->delete();
        return redirect('/order/transaction');
        
    }
    //
    public function index(){
        $data['transaksi'] = Transaksi::orderBy('created_at','desc')->paginate(5);
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['inboxe'] = \App\Inbox::count();
        $data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
        $data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
        $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		return view('Transaksi/transaksi',$data);
    }
    
    public function cari(Request $request)
    {
        //menangkap data pencarian
        $cari = $request->cari;

        //mengambil data dari table pegawai sesuai pencarian data
        $data['transaksi'] = Transaksi::where('pemilik','like',"%".$cari."%")
        ->paginate();
            $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
            $data['inboxe'] = \App\Inbox::count();
            $data['komentar'] = \DB::table('t_komentar')->where('delete_status','no')->count();
            $data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
            $data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();

        //mengirim data produk ke view index
        return view('Transaksi/transaksi',$data);
    }
    //

}
