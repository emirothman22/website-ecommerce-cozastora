<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;
class UserController extends Controller
{
    //
    public function index(){
        $data['user'] = \DB::table('users')->orderBy('created_at','desc')->paginate(5);
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
    	return view ('adminuser/user',$data);
    }
    public function delete($id)
    {
    	DB::table('users')->where('id',$id)->delete();
    	return redirect('/user')->with(['success' => 'Hapus Data Berhasil']);
    }
    public function cari(Request $request)
    {
        //menangkap data pencarian
        $cari = $request->cari;

        //mengambil data dari table pegawai sesuai pencarian data
        $data['user'] = DB::table('users')
        ->where('name','like',"%".$cari."%")
        ->paginate();
        $data['inbox'] = \DB::table('inbox')->orderBy('created_at','desc')->get();
		$data['totalnotifikasi'] = \DB::table('notifikasi')->where('status','unread')->count();
		$data['totalinbox'] = \DB::table('inbox')->where('status','unread')->count();

        //mengirim data produk ke view index
        return view('adminuser/user',$data);
    }
}
