<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use App\Gambar_Produk;
use App\Keranjang;

class UserLayoutController extends Controller
{
    //
    public function index(){
    
      if (Auth::check()) {
        # code...
        $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
        $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
        $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
        $data['banner'] = \DB::table('banner_admin')->get();
        $data['berita'] = \DB::table('berita_admin')->get();
        $data2['produk'] = Gambar_Produk::get();
        $data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
      } else {
        # code...
        $data['banner'] = \DB::table('banner_admin')->get();
        $data['berita'] = \DB::table('berita_admin')->get();
        $data2['produk'] = Gambar_Produk::get();
        $data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
      }
      
    
    return view('layoutUser/akunuser', $data,$data2);
  }
    public function index2(Request $request,$id)
  {
    $data['produk'] = \DB::table('produk')->find($id);
    $data['produks'] = \DB::table('produk')->get();
    $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
    $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
    return view('layoutUser/productdetail',$data);
  }
   public function editPengaturan(Request $request,$id)
  {
    $data['users'] = \DB::table('users')->find($id);
    $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
    $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
    return view('layoutUser/pengaturan',$data);
  }
  
    public function filterdata(Request $request)
    {
      if (Auth::check()) {
      $kategoriproduk = $request->jenis;
      $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
      $data['produk'] = Gambar_Produk::where('kategori_produk', '=' ,$kategoriproduk)->get();
      $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
      $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
      $data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
      $data2['banner'] = \DB::table('banner_admin')->get();
      $data['berita'] = \DB::table('berita_admin')->get();
      } else {
      $kategoriproduk = $request->jenis;
      $data['produk'] = Gambar_Produk::where('kategori_produk', '=' ,$kategoriproduk)->get();
      $data2['kategoriproduk'] = \DB::table('kategori_produk')->get();
      $data2['banner'] = \DB::table('banner_admin')->get();
      $data['berita'] = \DB::table('berita_admin')->get();
      }
      return view('layoutUser/akunuser', $data,$data2);
    }
    
}
