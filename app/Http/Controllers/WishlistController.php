<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wishlist;
use App\Gambar_Produk;
use Illuminate\Support\Facades\Auth;
use App\Keranjang;

class WishlistController extends Controller
{
    public function wishlist()
    {
        $data['harapan'] = Wishlist::where('pemilik',auth()->user()->name)->get();
        $data['totalsuka'] = Wishlist::where('pemilik',auth()->user()->name)->count();
        $data['keranjang'] = Keranjang::where('pemilik',auth()->user()->name)->orderBy('created_at','desc')->get();
        $data['totalkeranjang'] = Keranjang::where('pemilik',auth()->user()->name)->sum('kuantitas');
        return view('Wishlist/wishlist',$data);
    }

    public function addWishlist($id)
    {
        $produk = Gambar_Produk::find($id);
        $wishlist = Wishlist::where('pemilik', auth()->user()->name)->where('id_produk',$produk->id)->first();

        if ($wishlist) {
            return redirect()->back()->with('success','Produk ini telah disukai');
            # code...
            
        } else {
            Wishlist::create([
                'pemilik' => auth()->user()->name,
                'id_produk' => $produk->id,
                'nm_produk' => $produk->nm_produk,
                'harga' => $produk->harga,
                'gambar_produk' => $produk->gambar,
            ]);

            $produk->suka += 1;
            $produk->save();
        
            # code...
        }
        return redirect()->back()->with('success','Produk ini telah disukai');
        
    }
    public function remove($id)
    {
        $produk = Gambar_Produk::find($id);
        $count = $produk->suka -= 1;
        $produk->save();
        
        Wishlist::where('id_produk', $id)->delete();

        return redirect()->back()->with('success','Produk telah dihapus dari wishlist');
    }
    public static function likeExist($string)
    {
        if (Auth::check()) {

            return \DB::table('wishlist')->where('id_produk', $string)->where('pemilik', auth()->user()->name)->first();
        }   
    }
    //
}
