<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    //
    protected $table = 'inbox';
    protected $fillable = ['gambar','subjek','email','komentar','status'];
    protected $dates = [
    'created_at',
    'updated_at'];
}
