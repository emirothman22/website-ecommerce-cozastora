<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{

    protected $table = 'keranjang';
    protected $fillable = ['pemilik','id_produk','nm_produk','deskripsi_produk','gambar_produk','harga_produk'
    ,'kuantitas','berat_produk','stok_produk','total','status_pengiriman','kota'];
    //
}
