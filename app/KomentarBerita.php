<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarBerita extends Model
{
    protected $table = 't_komentar';
    protected $fillable = ['komentar','kategori','gambar','objek','rating','to_post','provider','name','email','website','delete_status'];
    
    //
}
