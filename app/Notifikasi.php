<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    protected $table = 'notifikasi';
    protected $fillable = ['id_produk','id_berita','gambar','objek','provider','subjek','kategori_notifikasi','status','komentar'];
    //
}
