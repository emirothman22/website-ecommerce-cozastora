<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = ['pemilik', 'kurir', 'jenis_layanan', 
    'metode_pembayaran', 'ongkir', 'total_harga', 'status_pembayaran', 'no_resi',];
    //
}
