<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 'wishlist';
    protected $fillable = ['pemilik','id_produk','nm_produk','gambar_produk','harga'];
    //
}
