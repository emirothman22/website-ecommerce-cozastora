<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingKomentar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     schema::table('t_komentar', function($table){
            $table->string('rating')->after('gambar')->nullable();
        });   
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('t_komentar', function($table){
            $table->dropColumn('rating');
        });
        //
    }
}
