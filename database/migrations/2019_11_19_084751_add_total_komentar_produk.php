<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalKomentarProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::table('produk', function($table){
            $table->string('total_komentar')->after('kuantitas')->nullable();
        });   
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('produk', function($table){
            $table->dropColumn('total_komentar');
        });   
        //
    }
}
