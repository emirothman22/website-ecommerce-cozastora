<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_admin', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->string('name');
            $table->string('gambar')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('showpassword');
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_admin');
    }
}
