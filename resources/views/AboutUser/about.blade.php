<!DOCTYPE html>
<html lang="en">
<head>
	<title>About Us</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('cozastore/images/icons/favicon.png')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/linearicons-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animate/animate.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body class="animsition">
	
	<!-- Header -->
  @include('AboutUser/header')
  	<!-- Cart -->
	<div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>

		<div class="header-cart flex-col-l p-l-65 p-r-25">
			<div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Your Cart
				</span>

				<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
					<i class="zmdi zmdi-close"></i>
				</div>
			</div>
			
			@if (Auth::check())
			<div class="header-cart-content flex-w js-pscroll">
				<ul class="header-cart-wrapitem w-full">
					@foreach ($keranjang->take(3) as $row)
					<li class="header-cart-item flex-w flex-t m-b-12">
						<form action="{{ url('/order/'.$row->id) }}" method="post">
							@method('DELETE')
							@csrf
						<div class="header-cart-item-img">
							<button type="submit"><img src="{{ url('uploadgambar') }}/{{ $row->gambar_produk }}" width="140px"></button>
						</div>
						</form>
						<div class="header-cart-item-txt p-t-8">
							<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
								{{$row->nm_produk}}
							</a>

							<span class="header-cart-item-info">
								{{$row->kuantitas}} x @currency($row->harga_produk)
							</span>
						</div>
					</li>
				@endforeach
				</ul>
				
				<div class="w-full">
					<div class="header-cart-total w-full p-tb-40">
						Total: $75.00
					</div>

					<div class="header-cart-buttons flex-w w-full">
						<a href="shoping-cart.html" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							View Cart
						</a>

						<a href="shoping-cart.html" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
							Check Out
						</a>
					</div>
				</div>
			</div>
			@else
				
			@endif
		</div>
	</div>

	
	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('{{asset('cozastore/images/bg-01.jpg')}}">
		<h2 class="ltext-105 cl0 txt-center">
			About
		</h2>
	</section>



	<!-- Content page -->
	<section class="bg0 p-t-75 p-b-120">
		<div class="container">
			<div class="row p-b-148">
				<div class="col-md-7 col-lg-8">
					<div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md">
						<h3 class="mtext-111 cl2 p-b-16">
							My Story
						</h3>

						<p class="stext-113 cl6 p-b-26">
							Saya Lahir dipalu, tepatnya tanggal 22 juni 2002.dan pada tahun 2012 saya memutuskan untuk pindah ke bandung untuk melanjutkan pendidikan sekolah dasar.pada saat itu saya pindah pada umur 10 tahun,ketikan saya sudah menyelasaikan pendidikan dibandung saya memutuskan untuk melnajutkan smp di jatim,jombang.saya disana menempuh ilmu akhirot sekaligus menempuh ilmu dunia.banyak pengalaman dan inspirasi saya saat disana.setelah saya pesantren dijatim,saya melanjutkan smk dibandung.saya sekolah di smkn4 bandung dan saya mengambil jurusan Rekayasa Perangkat Lunak.bagi saya ilmu komputer didalam diri daya tidak terlalu bagus,namun karena saya penasaran akan ilmu komputer/digital.akhirnya dengan senang hati saya mengambil jurusan rpl,walau nantinya saya akan mengalami hal yang tidak mengenakan namun itu harus dilawan.
						</p>

						<p class="stext-113 cl6 p-b-26">
							Tidak berasa akhirnya sudah kelas 3,dan akan menghadapi UN dan saya harus lebih giat belajar dan lebih ditekuni ilmu komputernya.dan saya berharap ketika saya udah lulus.saya bisa mencari pekerjaan selama 1 tahun,kemudian saya akan kembali belajar ilmu agama.
						</p>

						<p class="stext-113 cl6 p-b-26">
							Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
						</p>
					</div>
				</div>

				<div class="col-11 col-md-5 col-lg-4 m-lr-auto">
					<div class="how-bor1 ">
						<div class="hov-img0">
							<img src="{{asset('cozastore/images/tentang.jpg')}}" alt="IMG">
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="order-md-2 col-md-7 col-lg-8 p-b-30">
					<div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
						<h3 class="mtext-111 cl2 p-b-16">
							My Mission
						</h3>

						<p class="stext-113 cl6 p-b-26">
							Sebagai Programmer tentunya saya harus banyak belajar dan mencari pengalaman lebih dalam,yang mana tujuan saya tidak lain dan tidak bukan memberikan suasana baru untuk masyarakat,menciptakan suatu aplikasi yang memnag sangat dibutuhkan di era jaman sekarang.
							kreativitas yang kuat,inspirasi,dan membuat inovasi baru itu merupakan tujuan utama saya.
							tidak hanya sebuah aplikasi,namun saya berminat agar berkaloborasi dengan jurusan yang memang bisa menciptakan produk-produk yang menyenangkan.
						</p>

						<div class="bor16 p-l-29 p-b-9 m-t-22">
							<p class="stext-114 cl6 p-r-40 p-b-11">
								Sebuah Usaha dan doa merupakan hal yang paling penting dalam saya,dengan hal itu kita dapat memberikan suatu hasil yang bagus jika selalu ditekuni dan kerja keras.
							</p>

							<span class="stext-111 cl8">
								- Emir Othman
							</span>
						</div>
					</div>
				</div>

				<div class="order-md-1 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
					<div class="how-bor2">
						<div class="hov-img0">
							<img src="{{asset('cozastore/images/myemir.jpg')}}" alt="IMG">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	
	
		


	
		

	<!-- Footer -->
		@include('AboutUser/footer')



	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

<!--===============================================================================================-->	
	<script src="{{asset('cozastore/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('cozastore/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/select2/select2.min.js')}}"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/js/main.js')}}"></script>

</body>
</html>