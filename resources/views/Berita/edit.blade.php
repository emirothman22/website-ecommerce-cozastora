<!-- <!DOCTYPE html>
<html>
<head>
	<title>Form Edit Data Berita</title>
</head>
<body>
	<style type="text/css">
		body{
			margin: 0;
			padding: 0;
			font-family: sans-serif;
			letter-spacing: 1px;
			background-size: cover;
			background: url({{asset('cozastore/images/bgbiru.jpg')}}) no-repeat;

		}
		.form-input{
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
			width: 400px;
			height: 500px;
			padding: 80px 40px;
			box-sizing: border-box;
			background: rgba(0,0,0,.8);
			
			border-radius: 20px;
		}
		.form-input:before{
			content: '';
			position: absolute;
			top: 0;
			left: 0;
			width: 50%;
			height: 100%;
			background: rgba(255,255,255,.1);
			pointer-events: none;
		}
		.man{
			width: 100px;
			height: 100px;
			border-radius: 50%;
			position: absolute;
			top: calc(-100px/2);
			left: calc(50% - 50px);
			}
		h2{
			margin: 0;
			padding: 0 0 20px;
			color: #efed40;
			text-align: center;
		}
		.form-input p{
			margin: 0;
			padding: 0;
			font-weight: bold;
			color: #fff;
		}
		.form-input input{
			width: 100%;
			margin-bottom: 20px;
		}
		.form-input input[type="text"]
		{
			border: none;
			border-bottom: 1px solid #fff;
			background: transparent;
			outline: none;
			color: #fff;
			font-size: 16px;
			height: 40px;
		}
		.form-input input[type="submit"]
		{
			border: none;
			outline: none;
			color: #262626;
			background: #fbc531;
			cursor: pointer;
			border-radius: 20px;
			height: 40px;
			font-size: 16px;
		}
		.form-input input[type="submit"]:hover
		{
			background: #efed40;
			color: #262626;
		}
		.form-input a{
			color: #fff;
			font-size: 14px;
			font-weight: bold;
			text-decoration: none;
		}
		.form-input a:hover{
			color: #efed40;
		}		


	</style>
	<div class="form-input">
	<img src="{{asset('cozastore/images/images.png')}}" class="man">
	<h2>Edit Berita</h2>

	<br/>
	<br/>
	@foreach($berita as $pro)
	<form action="/berita/update" method="post">
		{{csrf_field()}}

		<input type="hidden" name="id" value="{{$pro->id_berita}}">
		<p class="textbox">Judul</p> 
		<input type="text" name="judul" required="required" value="{{$pro->judul}}">
		<p class="textbox">Gambar</p> 
		<input type="text" name="gambar" required="required" value="{{$pro->gambar}}">
		<p class="textbox">Deskripsi </p>
		<input type="text" name="deskripsi" required="required" value="{{$pro->deskripsi}}">
		<input type="submit" name="simpan" value="Simpan Data">
		<a href="/produk" >Kembali</a>

	</form>
</div>
	@endforeach

</body>
</html> -->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit Data</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('AdminLte/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
   <!--navabar-->
@include('Berita/header')
  <!--/.navbar-->

<!--sidebar-->
@include('Berita/sidebar')
<!--/sidebar-->





 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit berita</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Project Edit</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            @foreach($berita as $pro)

            <form action="/berita/update" method="post">
			{{csrf_field()}}

            <div class="card-body">
              <div class="form-group">
              <input type="hidden" name="id" value="{{$pro->id_berita}}">
                <label for="inputName">Judul</label>
                <input type="text" class="form-control" name="judul" id="inputName" required="required" 
                value="{{$pro->judul}}">
              </div>
              <div class="form-group">
                <label for="inputDescription">Deskripsi</label>
                <textarea name="deskripsi"  required="required" id="inputDescription" class="form-control" rows="4" 
                value="{{$pro->deskripsi}}"></textarea>
              </div>
              <div class="form-group">
                <p class="textbox"><b>Gambar</b></p>      
				<input type="file" name="gambar" required="required" placeholder="pilih gambar"  value="{{$pro->gambar}}">

              </div>
              <div class="row">
        <div class="col-12">
          <a href="/berita" type="button" class="btn btn-primary">Kembali</a>

          <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary" >
        </div>
      </div>
      
          
            </div>
        </form>
         @endforeach
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    
      </div>


    </section>
    <!-- /.content -->
  </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

</div>
<!--footer-->
@include('Berita/footer')
<!--/footer-->
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('AdminLte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('AdminLte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('AdminLte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('AdminLte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('AdminLte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('AdminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('AdminLte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('AdminLte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('AdminLte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
</body>
</html>