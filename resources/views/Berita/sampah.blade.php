<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('AdminLte/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <style type="text/css">
  .tbl{
        margin: 30px auto;
        width: 1050px;
        padding: 10px;
        text-align: center;
        margin-left: 100px;
        vertical-align: center;
        
  }
  .table table-striped{
    border-radius: 20px;
  }
  .cari-data{
    font-family: monospace;
    font-size: 50sp;
    text-align: left;
  }
  h3{
    text-align: left;
    font-family: monospace;
  }
  #cari-data{
    width: 100px;
    position: relative;
  }
  #input-produk{
    width: 200px;
    margin-left: 550px;

  }
  .linktambah{
    text-align: left;
  }
  .tombol-search
  {
   margin-left: 20px;
  }
  .data{
    margin-right: 79%;
    margin-top: -1%;

  }
  
  

</style>
<div class="wrapper">
   <!--navabar-->
  @include('Berita/header')
  <!--/.navbar-->

<!--sidebar-->
@include('Berita/sidebar')
<!--/sidebar-->





 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Berita</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Berita</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="tbl">
      <div class="data">
      <a href="/viewberita" class="btn btn-outline-success btn-sm">View Berita</a>
      |
      <a href="/viewberita/sampah"class="btn btn-outline-success btn-sm">Trash</a>
      </div>
    <br/>
    <br/>
    <a href="/viewberita/kembalikan_semua" class="btn btn-default btn-sm">Kembalikan Semua</a>
    |
    <a href="/viewberita/hapus_permanen_semua" class="btn btn-default btn-sm">Hapus Permanen Semua</a>
    <br/>
    <br/>
     @if(session('success'))
    <div class="alert alert-success">
      {{ session('succes') }}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-error">
      {{ session('error') }}
    </div>
    @endif
    <table class="table table-striped" >
      <thead class="thead-dark"> 
      <tr class="table-danger" >
        <td align="center" scope="col" class="bg-primary"><h6> No </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Judul </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Kategori Berita </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Gambar </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Caption </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Views </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Komentar </h6></td>
        <td align="center" scope="col" class="bg-danger"><h6> Aksi </h6></td>
     </tr>
      </thead>

    @foreach($berita as $row)
  <tr>
    <center>
    <td class="bg-light">{{ isset($i) ? ++$i : $i =1}} </td>
    <td class="bg-light">{{ $row->judul }}</td>
    <td class="bg-light">{{ $row->kategori_berita }}</td>
    <td class="bg-light"><img src="{{ url('uploadgambar') }}/{{ $row->gambar }}" width="200px"></td>
    <td class="bg-light">{{ $row->caption }}</td>
    <td class="bg-light">{{ $row->views }}</td>
    <td class="bg-light">{{ $row->total_komentar }}</td>


    <td>
      <center>
      <a class="btn btn-info btn-sm" href="/viewberita/kembalikan/{{ $row->id}}" style="margin-top:10%;"> Restore</a>
      <a class="btn btn-danger btn-sm" href="/viewberita/hapus_permanen/{{ $row->id }}" style="margin-top:10%;"> Delete </a>

    </td>


  </tr>
    @endforeach
</table>
{{ $berita->links() }}
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

</div>
<!--footer-->
@include('Berita/footer')
<!--/footer-->
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('AdminLte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('AdminLte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('AdminLte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('AdminLte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('AdminLte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('AdminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('AdminLte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('AdminLte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('AdminLte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
</body>
</html>



