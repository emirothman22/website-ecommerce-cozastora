<!DOCTYPE html>
<html lang="en">
<head>
	<title>Checkout</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('cozastore/images/icons/favicon.png')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/linearicons-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animate/animate.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body class="animsition">
	<style>
	.btn-delete{
		padding-right: 100px;
	}
	</style>
	
	<!-- Header -->
  @include('DetailOrder/header')

	<!-- Cart -->
	<div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>

		<div class="header-cart flex-col-l p-l-65 p-r-25">
			<div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Your Cart
				</span>

				<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
					<i class="zmdi zmdi-close"></i>
				</div>
			</div>
			
			@if (Auth::check())
			<div class="header-cart-content flex-w js-pscroll">		
				<ul class="header-cart-wrapitem w-full">
					@foreach ($keranjang->take(3) as $row)
					<li class="header-cart-item flex-w flex-t m-b-12">
						<form action="{{ url('/order/'.$row->id) }}" method="post">
							@method('DELETE')
							@csrf
						<div class="header-cart-item-img">
							<button type="submit"><img src="{{ url('uploadgambar') }}/{{ $row->gambar_produk }}" width="140px"></button>
						</div>
						</form>
						<div class="header-cart-item-txt p-t-8">
							<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
								{{$row->nm_produk}}
							</a>

							<span class="header-cart-item-info">
								{{$row->kuantitas}} x @currency($row->harga_produk)
							</span>
						</div>
					</li>
				@endforeach
				</ul>
				

				
				<div class="w-full">
					<div class="header-cart-total w-full p-tb-40">
						Total: $75.00
					</div>

					<div class="header-cart-buttons flex-w w-full">
						<a href="/order" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							View Cart
						</a>

						<a href="shoping-cart.html" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
							Check Out
						</a>
					</div>
				</div>
			</div>
							
			@else
				
			@endif
		</div>
	</div>
<br/>
<br/>

	<!-- breadcrumb -->
	<div class="container">
		<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
			<a href="/akunuser" class="stext-109 cl8 hov-cl1 trans-04">
				Home
				<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
			</a>

			<span class="stext-109 cl4">
                <a href="/order" class="stext-109 cl8 hov-cl1 trans-04">
                Shoping Cart
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                </a>
            </span>
            <span class="stext-109 cl4">
				Checkout
			</span>
		</div>
	</div>
		

	<!-- Shoping Cart -->
	<div class="bg0 p-t-75 p-b-85">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						<div class="wrap-table-shopping-cart">
							@if ($message = Session::get('success'))
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert"> × </button> 
								<strong>{{ $message }}</strong>
							</div>
                            @endif
						
						<form action="/order/checkout" method="post" novalidate="novalidate">
							@csrf
                        <div class="card">
                            <header class="card-header">
                                <h4 class="card-title mt-2">Billing Details</h4>
							</header>
							
                            <article class="card-body">
                                {{-- <div class="form-row">
                                    <div class="col form-group">
                                        <label>First name</label>
                                        <input type="text" class="form-control" name="first_name">
                                    </div>
                                    <div class="col form-group">
                                        <label>Last name</label>
                                        <input type="text" class="form-control" name="last_name">
                                    </div>
                                </div> --}}
                                {{-- <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address">
								</div> --}}
								{{-- <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Provinsi Asal</label>
                                        <select name="province_origin" class="form-control">
											<option value="">Provinsi</option>
											@foreach ($provinces as $province => $value)
												<option value="{{ $province }}">{{ $value }}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Provinsi Tujuan</label>
                                        <select name="province_destination" class="form-control" id="">
											<option value="">Provinsi Tujuan</option>
											@foreach ($provinces as $province => $value)
											<option value="{{ $province }}">{{ $value }}</option>
											@endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Kota Asal</label>
                                        <select name="city_origin" class="form-control">
                                            <option value="">Kota</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Kota Tujuan</label>
                                        <select name="city_destination" class="form-control" id="">
                                            <option value="">Kota</option>
                                        </select>
                                    </div>
								</div> --}}
								<div class= "form-group">
									<label for="">Kurir</label>
									<select name="kurir" class="form-control" id="">
										<option value="">Kurir</option>
										@foreach ($couriers as $courier => $value)
											<option value="{{ $courier }}">{{ $value }}</option>											
										@endforeach
									</select>
								</div>
								{{-- <div class="form-group">
									<label for="">Berat (g)</label>
									<input type="number" name="weight" id="" class="form-control" value="1000">
								</div> --}}
                                {{-- <div class="form-row">
                                    <div class="form-group  col-md-6">
                                        <label>Post Code</label>
                                        <input type="text" class="form-control" name="post_code">
                                    </div>
                                    <div class="form-group  col-md-6">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" name="phone_number">
                                    </div>
                                </div> --}}
                                {{-- <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" disabled>
                                    <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label>Order Notes</label>
                                    <textarea class="form-control" name="notes" rows="6"></textarea>
                                </div> --}}
							</article>
						 <button type="submit" class="btn btn-success form-control">Cek Ongkir</button>
						 <br>
						 <br>
						<table class="table-shopping-cart">
							<tbody>
								@foreach ($kurir as $kr)
									<tr>

									<td scope="row"><a href="{{ url('/order/shipping/'. $kr->id .'/pilih')}}"  class="zmdi zmdi-circle-o"></a><label for="{{$kr->id}}">{{$kr->skt_jenis_layanan}}</label></td>
									<td scope="row"><label for="{{$kr->id}}">@currency($kr->harga)</label></td>
									@if ($kr->skt_kurir === 'pos')
										<td scope="row"><label for="{{$kr->id}}">{{ $kr->estimasi}}</label></td>
									@else
									<td scope="row"><label for="{{$kr->id}}">{{ $kr->estimasi }} Hari</label></td>
										
									@endif
									</tr>
								@endforeach
							</tbody>
						</table>
						</div>
						</form>
                        </div>
					</div>
				</div>

			@if (Auth::check())
				<?php $total = 0 ?>
				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<form action="{{ url('/order/transaction')}}" method="post">
							@csrf
						<h4 class="mtext-109 cl2 p-b-30">
							Cart Totals
						</h4>
						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Subtotal:
								</span>
							</div>
							
							<div class="size-209">
								<span class="mtext-110 cl2">
									@currency($subtotal + $total)
								</span>
							</div>
						</div>
						<table  class="table table-borderless">
							 	<tr>
                                    <th class="head" colspan="2">Produk</th>
                                    <th class="head" style="text-align: right">Total</th>
                                </tr>
						{{-- <div class="flex-w flex-t bor12 p-t-15 p-b-30">
							<div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Shipping:
								</span>
								

								<span class="stext-110 cl2">
									Produk:
								</span>
								<span class="stext-110 cl2">
									 Kuantitas:
								</span>
								
								<br>
								<br>
							</div>
						</div> --}}
						@if (session('cart'))
						@foreach (session('cart') as $id => $details)
						<?php $total += $details['harga_produk'] * $details['kuantitas'] ?>	
						<tr>
                        	<?php $total += $details['harga_produk'] * $details['kuantitas'] ?>
                        	<td>{{ $details['nm_produk'] }}</></td>
                        	<td class="produk">x {{ $details['kuantitas'] }}</td>
                        	<td class="produk" align="right">Rp.{{ $details['harga_produk'] * $details['kuantitas'] }}</td>
                        </tr>
						
						@endforeach
						@foreach ($keranjang as $row)
						<tr>
							 <td>{{ $row->nm_produk }}</></td>
                             <td class="produk">x {{$row->kuantitas }}</td>
                             <td class="produk" align="right">@currency($row['harga_produk'] * $row['kuantitas'])</td>	
						</tr>							
						@endforeach
						@else
						@foreach ($keranjang as $row)
						<tr>
							 <td>{{ $row->nm_produk }}</></td>
                             <td class="produk">x {{ $row->kuantitas }}</td>
                             <td class="produk" align="right">@currency($row['harga_produk'] * $row['kuantitas'])</td>	
						</tr>		
						@endforeach
						@endif
						
						
						</table>
						@if (session('ongkir'))
						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Ongkir:
								</span>
							</div>
							
							<div class="size-209">
								<span class="mtext-110 cl2">
									Rp.<input style="margin-left:20%; margin-top:-12%"  type="number" value="{{session()->get('ongkir')[2]}}" name="ongkir" readonly>
								</span>
							</div>
						</div>	
						@endif
						
						<div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Total:
								</span>
							</div>

							<div class="size-209 p-t-1">
								<span class="mtext-110 cl2">
									Rp.<input type="text" style="margin-left:20%; margin-top:-12%" value="{{ $subtotal + $total + session()->get('ongkir')[2] }}" class="total" name="total" readonly>
								</span>
							</div>
						</div>

						@if (session('ongkir'))
							<input type="hidden" value="{{ strtoupper(session('ongkir')[0]) }}" name="kurir">
							<input type="hidden" value="{{ session('ongkir')[1] }}" name="jenis_layanan">
						
						<button type="submit" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
							 Order
						</button>
						@endif
					</form>
						
					</div>
				</div>	
				@else
					
				@endif
			</div>
		</div>
	</div>
		
	
		

	<!-- Footer -->
		@include('DetailOrder/footer')



	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

<!--===============================================================================================-->	
	<script src="{{asset('cozastore/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('cozastore/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script>
	$(document).ready(function() {
		$('select[name="province_origin"]').on('change',function(){
			let provinceId = $(this).val();
			if (provinceId) {
				jQuery.ajax({
					url: '/province/'+provinceId+'/cities',
					type:"GET",
					dataType:"json",
					success:function(data){
						$('select[name="city_origin"]').empty();
						$.each(data, function(key, value){
							$('select[name="city_origin"]').append('<option value="'+ key +'">' + value + '</option>');
						});
					},
				});
			} else {
				$('select[name="city_origin"]').empty();
			}
		});

		$('select[name="province_destination"]').on('change',function(){
			let provinceId = $(this).val();
			if (provinceId) {
				jQuery.ajax({
					url:'/province/'+provinceId+'/cities',
					type:'GET',
					dataType:'json',
					success:function(data) {
						$('select[name="city_destination"]').empty();
						$.each(data, function(key, value){
							$('select[name="city_destination"]').append('<option value="'+ key +'">' + value + '</option>');
						});
					},
				});
			} else {
				$('select[name="city_destination"]').empty();
			}
		});
	});
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/select2/select2.min.js')}}"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});

		//remove cart for session
		$(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Yakin untuk menghapus ?")) {
                $.ajax({
                    url: '{{ url('order/remove/session') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

		//update from session
		$(".update-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Yakin untuk ingin mengubah ?")) {
                $.ajax({
                    url: '{{ url('order/update/session') }}',
                    method: "PATCH",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/js/main.js')}}"></script>

</body>
</html>