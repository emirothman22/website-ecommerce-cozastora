<!DOCTYPE html>
<html lang="en">
<head>
	<title>Konfirmasi Pembayaran</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('cozastore/images/icons/favicon.png')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/linearicons-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animate/animate.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body class="animsition">
	<style>
	.btn-delete{
		padding-right: 100px;
	}
	</style>
	
	<!-- Header -->
  @include('DetailOrder/header')

	<!-- Cart -->
	<div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>

		<div class="header-cart flex-col-l p-l-65 p-r-25">
			<div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Your Cart
				</span>

				<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
					<i class="zmdi zmdi-close"></i>
				</div>
			</div>
			
			@if (Auth::check())
			<div class="header-cart-content flex-w js-pscroll">		
				<ul class="header-cart-wrapitem w-full">
					@foreach ($keranjang->take(3) as $row)
					<li class="header-cart-item flex-w flex-t m-b-12">
						<form action="{{ url('/order/'.$row->id) }}" method="post">
							@method('DELETE')
							@csrf
						<div class="header-cart-item-img">
							<button type="submit"><img src="{{ url('uploadgambar') }}/{{ $row->gambar_produk }}" width="140px"></button>
						</div>
						</form>
						<div class="header-cart-item-txt p-t-8">
							<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
								{{$row->nm_produk}}
							</a>

							<span class="header-cart-item-info">
								{{$row->kuantitas}} x @currency($row->harga_produk)
							</span>
						</div>
					</li>
				@endforeach
				</ul>
				

				
				<div class="w-full">
					<div class="header-cart-total w-full p-tb-40">
						Total: @currency($total)
					</div>

					<div class="header-cart-buttons flex-w w-full">
						<a href="/order" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							View Cart
						</a>
                        @if ($cek_keranjang !=0 || session('cart') != 0)
						<a href="/order/checkout" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
							Check Out
                        </a>
                        @endif
					</div>
				</div>
			</div>
							
			@else
				
			@endif
		</div>
	</div>
<br/>
<br/>

	<!-- breadcrumb -->
	<div class="container">
		<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
			<a href="/akunuser" class="stext-109 cl8 hov-cl1 trans-04">
				Home
				<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
			</a>

			<span class="stext-109 cl4">
				konfirmasi pembayaran
			</span>
		</div>
	</div>
		

	<!-- Shoping Cart -->
	<p class="flex-w flex-t p-t-27 p-b-33"><h4><center>Thanks For Your Buying</h4></p>
                    <br>
	<div class="bg0 p-t-75 p-b-85">
        
		<div class="container">
			<div class="row">
				        <div class="card" style="margin-left:20%">
                            <header class="card-header">
                                <h4 class="card-title mt-2">Details Order</h4>
							</header>
                            <article class="card-body">

								<table class="table table-borderless">
                                    <tr>
                                        <td>Nomor Resi :</td>
                                        <td>: {{$transaksi->no_resi }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>: {{ date('d-m-y', strtotime($transaksi->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>: @currency ($transaksi->total_harga)</td>
                                    </tr>
                                    <tr>
                                        <td>Metode Pembayaran</td>
                                        <td>: {{ $transaksi->metode_pembayaran }}</td>
                                    </tr>
                                </table>
								
							</article>
						 
                        </div>
                        
                <div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Order Totals
						</h4>
						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Subtotal:
								</span>
							</div>
							
							<div class="size-209">
								<span class="mtext-110 cl2">
									@currency($subtotal)
								</span>
							</div>
						</div>
						<table  class="table table-borderless">
                            <thead>
							 	<tr>
                                    <th class="head">Produk</th>
                                    <th>Kuantitas</th>
                                    <th class="head" style="text-align: right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pesanan as $row)
                                    <tr>
                                        <td>
                                            <p>{{ $row->nama_produk }}</p>
                                        </td>
                                        <td>
                                            <h5>x {{ $row->kuantitas }}</h5>
                                        </td>
                                        <td>
                                            <p>{{ $row->total_harga }}</p>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
						
						</table>
						@if (session('ongkir'))
						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Ongkir:
								</span>
							</div>
							
							<div class="size-209">
								<span class="mtext-110 cl2">
									Rp.<input style="margin-left:20%; margin-top:-12%"  type="number" value="{{session()->get('ongkir')[2]}}" name="ongkir" readonly>
								</span>
							</div>
						</div>	
						@endif
						
						<div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Total:
								</span>
							</div>

							<div class="size-209 p-t-1">
								<span class="mtext-110 cl2">
									@currency($subtotal + $transaksi->ongkir)
								</span>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
		
	
		

	<!-- Footer -->
		@include('DetailOrder/footer')



	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

<!--===============================================================================================-->	
	<script src="{{asset('cozastore/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('cozastore/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script>
	// $(document).ready(function() {
	// 	$('select[name="province_origin"]').on('change',function(){
	// 		let provinceId = $(this).val();
	// 		if (provinceId) {
	// 			jQuery.ajax({
	// 				url: '/province/'+provinceId+'/cities',
	// 				type:"GET",
	// 				dataType:"json",
	// 				success:function(data){
	// 					$('select[name="city_origin"]').empty();
	// 					$.each(data, function(key, value){
	// 						$('select[name="city_origin"]').append('<option value="'+ key +'">' + value + '</option>');
	// 					});
	// 				},
	// 			});
	// 		} else {
	// 			$('select[name="city_origin"]').empty();
	// 		}
	// 	});

	// 	$('select[name="province_destination"]').on('change',function(){
	// 		let provinceId = $(this).val();
	// 		if (provinceId) {
	// 			jQuery.ajax({
	// 				url:'/province/'+provinceId+'/cities',
	// 				type:'GET',
	// 				dataType:'json',
	// 				success:function(data) {
	// 					$('select[name="city_destination"]').empty();
	// 					$.each(data, function(key, value){
	// 						$('select[name="city_destination"]').append('<option value="'+ key +'">' + value + '</option>');
	// 					});
	// 				},
	// 			});
	// 		} else {
	// 			$('select[name="city_destination"]').empty();
	// 		}
	// 	});
	// });
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/select2/select2.min.js')}}"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
	<script type="text/javascript">
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
		//update from session
		$(".update-from-cart").click(function (e) {
           e.preventDefault();

           var ele = $(this);
            $.ajax({
               url: '{{ url('order/update/session') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
			
        });

		//remove cart for session
		$(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Yakin untuk menghapus ?")) {
                $.ajax({
                    url: '{{ url('order/remove/session') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

		
		
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/js/main.js')}}"></script>

</body>
</html>