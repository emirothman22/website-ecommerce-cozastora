<!DOCTYPE html>
<html lang="en">
<head>
	<title>Add To Cart</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('cozastore/images/icons/favicon.png')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/fonts/linearicons-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animate/animate.css')}}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('cozastore/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body class="animsition">
	<style>
	.btn-delete{
		padding-right: 100px;
	}
	</style>
	
	<!-- Header -->
  @include('DetailOrder/header')

	<!-- Cart -->
	<div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>

		<div class="header-cart flex-col-l p-l-65 p-r-25">
			<div class="header-cart-title flex-w flex-sb-m p-b-8">
				<span class="mtext-103 cl2">
					Your Cart
				</span>

				<div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
					<i class="zmdi zmdi-close"></i>
				</div>
			</div>
			
			@if (Auth::check())
			<div class="header-cart-content flex-w js-pscroll">		
				<ul class="header-cart-wrapitem w-full">
					@foreach ($keranjang->take(3) as $row)
					<li class="header-cart-item flex-w flex-t m-b-12">
						<form action="{{ url('/order/'.$row->id) }}" method="post">
							@method('DELETE')
							@csrf
						<div class="header-cart-item-img">
							<button type="submit"><img src="{{ url('uploadgambar') }}/{{ $row->gambar_produk }}" width="140px"></button>
						</div>
						</form>
						<div class="header-cart-item-txt p-t-8">
							<a href="#" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
								{{$row->nm_produk}}
							</a>

							<span class="header-cart-item-info">
								{{$row->kuantitas}} x @currency($row->harga_produk)
							</span>
						</div>
					</li>
				@endforeach
				</ul>
				

				
				<div class="w-full">
					<div class="header-cart-total w-full p-tb-40">
						Total: $75.00
					</div>

					<div class="header-cart-buttons flex-w w-full">
						<a href="/order" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
							View Cart
						</a>

						<a href="shoping-cart.html" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
							Check Out
						</a>
					</div>
				</div>
			</div>
							
			@else
				
			@endif
		</div>
	</div>
<br/>
<br/>

	<!-- breadcrumb -->
	<div class="container">
		<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
			<a href="/akunuser" class="stext-109 cl8 hov-cl1 trans-04">
				Home
				<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
			</a>

			<span class="stext-109 cl4">
				Shoping Cart
			</span>
		</div>
	</div>
		

	<!-- Shoping Cart -->
	
	<div class="bg0 p-t-75 p-b-85">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						<div class="wrap-table-shopping-cart">
							@if ($message = Session::get('success'))
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert"> × </button> 
								<strong>{{ $message }}</strong>
							</div>
							@endif
							@if ($message = Session::get('message'))
							<div class="alert alert-success alert-block">
								<button type="button" class="close" data-dismiss="alert"> × </button> 
								<strong>{{ $message }}</strong>
							</div>
							@endif
					
							<table class="table-shopping-cart">
								<tr class="table_head">
									<th class="column-1">Product</th>
									<th class="column-2"></th>
									<th class="column-3" style="width:30%; padding-left:5%;">Price</th>
									<th class="column-4" style="text-align:center;  width:30%; text-align:center;">Quantity</th>
									<th class="column-5" style="text-align:center; padding-left:3%; width:50% " >SubTotal</th>
									<th class="column-6" colspan="2" style="text-align:center;  width:30%">Aksi</th>
								</tr>
								
								@if (Auth::check())
								
								<?php $total = 0 ?>
								@if (session('cart'))
									@foreach (session('cart') as $id => $check)

									<?php $total += $check['harga_produk'] * $check['kuantitas'] ?>

									<tr class="table_row">
									<td class="column-1">
										<div class="how-itemcart1">
											<br>
											<img src="{{ url('uploadgambar') }}/{{ $check['gambar_produk'] }}" width="140px">
											<br>
											@if (session('cart'))
												Session
											@endif
										</div>
									</td>
									<td class="column-2">{{$check['nm_produk'] }}</td>
									<td class="column-3 text-center" style="padding-left:3%">{{$check['harga_produk']}}</td>
									<td class="column-4 text-center" style="padding-left:2%">
										<div class="wrap-num-product flex-w m-l-auto m-r-0">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus"></i>
											</div>

											<input class="mtext-104 cl3 txt-center num-product quantity" type="number"  name="kuantitas{{ isset($i) ? ++$i : $i = 1 }}" min ="1" value="{{$check['kuantitas'] }}">

											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>
									</td>
									<td class="column-5  text-center" style="padding-left:1%">@currency($check['harga_produk'] * $check['kuantitas'])</td>
									<td class="column-6"><button data-id="{{ $id }} " class="btn btn-info btn-sm update-from-cart">Update</button>
									<td class="column-6"><button data-id="{{ $id }}" class="btn btn-danger btn-sm remove-from-cart">Remove
								</tr>	
								@endforeach
								@endif
								@foreach ($keranjang as $item)
								<tr class="table_row">
									<form action="{{ url('/order', $item->id) }}" method="POST">
										@csrf
										
										@if (!empty($keranjang))
											@method('PATCH')
										@endif
									<td class="column-1">
										<div class="how-itemcart1">
											<img src="{{ url('uploadgambar') }}/{{ $item->gambar_produk }}" width="140px">
										</div>
									</td>
									<td class="column-2">{{$item->nm_produk}}</td>
									<td class="column-3 text-center" style="padding-left:3%">{{$item->harga_produk}}</td>
									<td class="column-4  text-center" style="padding-left:2%">
										<div class="wrap-num-product flex-w m-l-auto m-r-0">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus"></i>
											</div>

											<input class="mtext-104 cl3 txt-center num-product" type="number" name="kuantitas" value="{{$item->kuantitas}}" min="0">

											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>
									</td>
									<td class="column-5  text-center" style="padding-left:1%">@currency($item['harga_produk'] * $item['kuantitas'])</td>
									<td class="column-6"><button type="submit" class="btn btn-info btn-sm">Update</button>
									</td>
									</form>	
									<form action="{{ url('/order', $item->id_produk) }}" method="POST">
										@method('DELETE')
										@csrf
										<div class="btn-delete">
									<td class="column-6"><button type="submit" class="btn btn-danger btn-sm">Remove</button>
									</td>
										</div>

									</form>
								</tr>
								
								@endforeach

								@else
								
								<?php $total = 0 ?>
								@if (session('cart'))
									@foreach (session('cart') as $id => $check)

									<?php $total += $check['harga_produk'] * $check['kuantitas'] ?>

								<tr class="table_row">
									<td class="column-1">
										<div class="how-itemcart1">
											<img src="{{ url('uploadgambar') }}/{{ $check['gambar_produk'] }}" width="140px">
										</div>
									</td>
									<td class="column-2">{{$check['nm_produk'] }}</td>
									<td class="column-3">{{$check['harga_produk']}}</td>
									<td class="column-4">
										<div class="wrap-num-product flex-w m-l-auto m-r-0">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus"></i>
											</div>

											<input class="mtext-104 cl3 txt-center num-product quantity" type="number" value="{{$check['kuantitas'] }}" min ="1">

											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>
									</td>
									<td class="column-5">@currency($check['harga_produk'] * $check['kuantitas'])</td>
									<td class="column-6"><button data-id="{{ $id }}" class="btn btn-info btn-sm update-from-cart">Update</button>
									<td class="column-7"><button data-id="{{ $id }}" class="btn btn-danger btn-sm remove-from-cart">Remove
								</tr>		
								@endforeach
								@endif
								@endif
								
									
								
							</table>
						</div>

						<div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
							<div class="flex-w flex-m m-r-20 m-tb-5">
								<input class="stext-104 cl2 plh4 size-117 bor13 p-lr-20 m-r-10 m-tb-5" type="text" name="coupon" placeholder="Coupon Code">
									
								<div class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5">
									Apply coupon
								</div>
							</div>

							<a href="#"><div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
								Update Cart
							</div>
						</a>
						</div>
					</div>
				</div>

				@if (Auth::check())
				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Cart Totals
						</h4>
						<form action="/order/process-checkout" method="post">
							@csrf

						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Subtotal:
								</span>
							</div>

							<div class="size-209">
								<span class="mtext-110 cl2">
									@currency($subtotal + $total)
								</span>
							</div>
						</div>

						<div class="flex-w flex-t bor12 p-t-15 p-b-30">
							<div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Shipping:
								</span>
								
							</div>

							<div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
								<p class="stext-111 cl6 p-t-2">
									There are no shipping methods available. Please double check your address, or contact us if you need any help.
								</p>

							
							
								
								<div class="p-t-15">
									<span class="stext-112 cl8">
										{{-- Calculate Shipping --}}
									</span>
								
{{-- 
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="province_origin">
											<option>Province From...</option>
											@foreach ($provinces as $province => $value)
												<option value="{{ $province }}">{{ $value }}</option>
											@endforeach
										</select>
										<div class="dropDownSelect2"></div>
									</div>
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="province_destination">
											<option>Province Destination...</option>
											@foreach ($provinces as $province => $value)
												<option value="{{ $province }}">{{ $value }}</option>
											@endforeach
										</select>
										<div class="dropDownSelect2"></div>
									</div>
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="city_origin">
											<option>City From...</option>		
										</select>
										<div class="dropDownSelect2"></div>
									</div>
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="city_destination">
											<option>City Destination...</option>		
										</select>
										<div class="dropDownSelect2"></div>
									</div> --}}

									{{-- <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="courier">
											<option>Courier...</option>
											@foreach ($couriers as $courier => $value)
											<option value="{{ $courier }}">{{ $value }}</option>											
											@endforeach		
										</select>
										<div class="dropDownSelect2"></div>
									</div> --}}
{{-- 
									<div class="bor8 bg0 m-b-12">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="number" name="weight" placeholder="Weight(g)" value="1000">
									</div>

									<div class="bor8 bg0 m-b-22">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="postcode" placeholder="Postcode / Zip">
									</div> --}}
									
																		
									{{-- <div class="flex-w">
										<div class="flex-c-m stext-101 cl2 size-115 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer">
											Update Totals
										</div>
									</div> --}}
										
								</div>
							</div>
						</div>

						{{-- <div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Total:
								</span>
							</div>

							<div class="size-209 p-t-1">
								<span class="mtext-110 cl2">
									@currency($subtotal + $total)
								</span>
							</div>
						</div> --}}

						@if ($cek_keranjang !=0 || session('cart') != 0)
						<a href="/order/checkout" class="flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
							 Checkout
						</a>
						@endif
					</form>
					</div>
				</div>	
				@else
				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Cart Totals
						</h4>
						
						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Subtotal:
								</span>
							</div>

							<div class="size-209">
								<span class="mtext-110 cl2">
									@currency($total)
								</span>
							</div>
						</div>

						<div class="flex-w flex-t bor12 p-t-15 p-b-30">
							<div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Shipping:
								</span>
							</div>

							<div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
								<p class="stext-111 cl6 p-t-2">
									There are no shipping methods available. Please double check your address, or contact us if you need any help.
								</p>
								
								<div class="p-t-15">
									<span class="stext-112 cl8">
										Calculate Shipping
									</span>

									{{-- <div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="province_origin">
											<option>Province From...</option>
											@foreach ($provinces as $province => $value)
												<option value="{{ $province }}">{{ $value }}</option>
											@endforeach
										</select>
										<div class="dropDownSelect2"></div>
									</div>
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="province_destination">
											<option>Province Destination...</option>
											@foreach ($provinces as $province => $value)
												<option value="{{ $province }}">{{ $value }}</option>
											@endforeach
										</select>
										<div class="dropDownSelect2"></div>
									</div>
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="city_origin">
											<option>City From...</option>		
										</select>
										<div class="dropDownSelect2"></div>
									</div>
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="city_destination">
											<option>City Destination...</option>		
										</select>
										<div class="dropDownSelect2"></div>
									</div> --}}
									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select class="js-select2" name="courier">
											<option>Courier...</option>
											@foreach ($couriers as $courier => $value)
											<option value="{{ $courier }}">{{ $value }}</option>											
											@endforeach		
										</select>
										<div class="dropDownSelect2"></div>
									</div>

									<div class="bor8 bg0 m-b-12">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="number" name="weight" placeholder="Weight(g)" value="1000">
									</div>

									<div class="bor8 bg0 m-b-22">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="postcode" placeholder="Postcode / Zip">
									</div>
									
																		
									{{-- <div class="flex-w">
										<div class="flex-c-m stext-101 cl2 size-115 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer">
											Update Totals
										</div>
									</div> --}}
										
								</div>
							</div>
						</div>

						{{-- <div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Total:
								</span>
							</div>

							<div class="size-209 p-t-1">
								<span class="mtext-110 cl2">
									$79.65
								</span>
							</div>
						</div> --}}

						<a href = "/login" type="submit" class="flex-w flex-t bor12 p-t-15 p-b-30">
							 Ingin Order??? Silahkan Login Dulu
						</a>
					</div>
				</div>	
				@endif

				
			</div>
		</div>
	</div>
		
	
		

	<!-- Footer -->
		@include('DetailOrder/footer')



	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

<!--===============================================================================================-->	
	<script src="{{asset('cozastore/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('cozastore/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script>
	// $(document).ready(function() {
	// 	$('select[name="province_origin"]').on('change',function(){
	// 		let provinceId = $(this).val();
	// 		if (provinceId) {
	// 			jQuery.ajax({
	// 				url: '/province/'+provinceId+'/cities',
	// 				type:"GET",
	// 				dataType:"json",
	// 				success:function(data){
	// 					$('select[name="city_origin"]').empty();
	// 					$.each(data, function(key, value){
	// 						$('select[name="city_origin"]').append('<option value="'+ key +'">' + value + '</option>');
	// 					});
	// 				},
	// 			});
	// 		} else {
	// 			$('select[name="city_origin"]').empty();
	// 		}
	// 	});

	// 	$('select[name="province_destination"]').on('change',function(){
	// 		let provinceId = $(this).val();
	// 		if (provinceId) {
	// 			jQuery.ajax({
	// 				url:'/province/'+provinceId+'/cities',
	// 				type:'GET',
	// 				dataType:'json',
	// 				success:function(data) {
	// 					$('select[name="city_destination"]').empty();
	// 					$.each(data, function(key, value){
	// 						$('select[name="city_destination"]').append('<option value="'+ key +'">' + value + '</option>');
	// 					});
	// 				},
	// 			});
	// 		} else {
	// 			$('select[name="city_destination"]').empty();
	// 		}
	// 	});
	// });
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/select2/select2.min.js')}}"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
	<script type="text/javascript">
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
		//update from session
		$(".update-from-cart").click(function (e) {
           e.preventDefault();

           var ele = $(this);
            $.ajax({
               url: '{{ url('order/update/session') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
			
        });

		//remove cart for session
		$(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if(confirm("Yakin untuk menghapus ?")) {
                $.ajax({
                    url: '{{ url('order/remove/session') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

		
		
	</script>
<!--===============================================================================================-->
	<script src="{{asset('cozastore/js/main.js')}}"></script>

</body>
</html>