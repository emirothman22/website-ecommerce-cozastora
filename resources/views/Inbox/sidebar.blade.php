  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('AdminLTE/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('AdminLTE/dist/img/user8-128x128.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Emir Othman</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        

            <!--For Banner-->

            <li class="nav-item">
            <a href="banner" class="nav-link ">
              <i class="far fa-images"></i>
              <p>
                Banner
                
              </p>
            </a>
          </li>

            <!---For User-->
            <li class="nav-item">
            <a href="user" class="nav-link">
             <i class="fas fa-users"></i>
              <p>
                User
                
              </p>
            </a>
          </li>
          

          <!--Produk-->
            
          <li class="nav-item">
            <a href="produk" class="nav-link">
            <i class="fas fa-store"></i>              
            <p>
                Produk
              </p>
            </a>
          </li>

           <!--Produk-->
            
          <li class="nav-item">
            <a href="/cart" class="nav-link">
            <i class="fas fa-cart-plus"></i>              
            <p>
                Cart
              </p>
            </a>
          </li>

          <!--Kategori produk-->
              <li class="nav-item">
            <a href="kategori" class="nav-link">
            <i class="fas fa-tags"></i>              
            <p>
                Kategori Produk
              </p>
            </a>
          </li>
               <!--  <nav class="mt-2">
               <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

              <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link ">
              <i class="fas fa-tags"></i>
              <p>
                Kategori Produk
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pakaian </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Elektronik </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Aksesoris </p>
                </a>
              </li>
            </ul>
          </li>
 -->

          <!--Transaksi-->
          <li class="nav-item">
            <a href="transaksi" class="nav-link">
              <i class="fas fa-shopping-cart"></i>
              <p>
                Transaksi
                
              </p>
            </a>
          </li>

           <!--kategori berita-->
          <li class="nav-item">
            <a href="/kategoriberita" class="nav-link">
            <i class="fas fa-pager"></i>          
            <p>
                Kategori Berita
              </p>
            </a>
          </li>

          <!--Berita-->
          <li class="nav-item">
            <a href="/viewberita" class="nav-link">
              <i class="far fa-newspaper"></i>
              <p>
                Berita
                
              </p>
            </a>
          </li>


          <!--About-->
          <li class="nav-item">
            <a href="/about_admin" class="nav-link">
             <i class="fas fa-exclamation-circle"></i>
              <p>
                About 
                
              </p>
            </a>
          </li>

          <!--Inbox-->
          {{-- <li class="nav-item">
            <a href="/inbox" class="nav-link active">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Inbox
                
              </p>
            </a>
          </li> --}}

          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Mailbox
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/inbox" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inbox</p>
                </a>
              </li>
              {{-- <li class="nav-item">
                <a href="/compose" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Compose</p>
                </a>
              </li> --}}
              <li class="nav-item">
                <a href="/read" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Read</p>
                </a>
              </li>
            </ul>
          </li>

           <!--Notifikasi-->
          <li class="nav-item">
            <a href="/notifikasi" class="nav-link">
             <i class="fas fa-bell"></i>
              <p>
                Notifikasi 
                
              </p>
            </a>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
      <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </aside>