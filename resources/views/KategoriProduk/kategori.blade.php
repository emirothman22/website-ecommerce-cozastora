<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Kategori</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('AdminLte/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">

   <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body align="center" class="hold-transition sidebar-mini layout-fixed">
  <style type="text/css">
  .tbl{
        margin: 20px auto;
        width: 1000px;
        padding: 10px;
        text-align: center;
        margin-left: 100px;
        vertical-align: center;
        
  }
  .table table-striped{
    border-radius: 20px;
  }
  .cari-data{
    font-family: monospace;
    font-size: 50sp;
    text-align: left;
  }
  h3{
    text-align: left;
  }
  #cari-data{
    width: 100px;
    position: relative;
  }
  #input-produk{
    width: 200px;
     margin-left: 550px;
  }
  


  
  

</style>
<div class="wrapper">
   <!--navabar-->
  @include('KategoriProduk/header')
  <!--/.navbar-->

<!--sidebar-->
@include('KategoriProduk/sidebar')
<!--/sidebar-->





 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Kategori Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Kategori Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

     <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->


        <!--Membuat Tabel Produk-->


  <div class="tbl">
    <form action="/kategori/cari" method="GET" align="left">
      <table>
        <div class="text">
        <td><a href="/kategori/tambah"  class="btn btn-info"><i class="fas fa-plus"></i> Tambah</a></td>
        </div>
      {{-- <td><input id="input-produk" class="form-control" type="text" name="cari" placeholder="Cari Kategori...." value="{{old('cari')}}"></td>
      <br/>
      <td><input type="submit" value="CARI" class="btn btn-primary" id="cari-data"></td> --}}
    </table>
    </form>
    <br/>
    @if(session('success'))
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert"> × </button> 
      {{ session('success') }}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-error">
      {{ session('error') }}
    </div>
    @endif    
    <table class="table table-striped" >
      <thead class="thead-dark"> 
      <tr class="table-danger" >
        <td align="center" scope="col" class="bg-primary"><h6> No </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Jenis Produk </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Size </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Color </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Gambar </h6></td>
        <td align="center" scope="col" class="bg-danger" id="tabel-aksi" style="width: 20%;"><h6> Aksi </h6></td>
     </tr>
      </thead>

    @foreach($kategoriproduk as $row)
  <tr>
    <center>
    <td class="bg-light">{{ isset($i) ? ++$i : $i =1}} </td>
    <td class="bg-light" >{{ $row->jenis }}</td>
    <td class="bg-light" >{{ $row->size }}</td>
    <td class="bg-light" >{{ $row->color }}</td>
    <td class="bg-light" ><img src="{{ url('kategorigambar') }}/{{ $row->gambar }}" width="140px"></td>
    <td>  
      <center>
      <a class="btn btn-info btn-sm" href="/kategori/edit/{{ $row->id}}"><i class="fas fa-pencil-alt">
      </i> Edit </a>
      <a class="btn btn-danger btn-sm" href="/kategori/hapus/{{ $row->id }}"><i class="fas fa-trash">
      </i> Hapus </a>
      </div>     
    </td>
  </tr>
    @endforeach
</table>
{{ $kategoriproduk->links() }}




<!--end table produk-->


        <!-- /.row -->
        <!-- Main row -->
        
            <!-- /.card -->

            <!-- DIRECT CHAT -->
            
              <!-- /.card-header -->
              
            <!--/.direct-chat -->

            <!-- TO DO List -->
            
              <!-- /.card-body -->
              
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
         
            <!-- /.card -->

            <!-- Calendar -->
            
            <!-- /.card -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

</div>
<!--footer-->
@include('KategoriProduk/footer')
<!--/footer-->
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('AdminLte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('AdminLte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('AdminLte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('AdminLte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('AdminLte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('AdminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('AdminLte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('AdminLte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('AdminLte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
</body>
</html>