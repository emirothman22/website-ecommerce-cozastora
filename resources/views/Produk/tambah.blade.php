<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tambah Data</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/summernote/summernote-bs4.css')}}">

   <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('AdminLte/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
   <!--navabar-->
@include('Produk/header')

<!--sidebar-->
@include('Produk/sidebartambah')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    
    <!-- Main content -->
     <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Bootstrap WYSIHTML5
                <small>Simple and fast</small>
              </h3>
            </div>
            @if(session('success'))
              <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"> × </button> 
                {{ session('success') }}
              </div>
             @endif
            @if(session('error'))
            <div class="alert alert-error">
              {{ session('error') }}
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Perhatikan</strong>
              <br/>
              <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
            @endif
            <!-- /.card-header -->
            <form action = "{{ url('/produk',@$produk->id) }}" method="post" enctype="multipart/form-data">
            @csrf

           @if(!empty($produk))
            @method('PATCH')
           @endif

            <div class="card-body">
            <div class="form-group">
                <label for="inputName">Nama Produk</label>
                <input type="text" class="form-control" name="nm_produk" id="inputName" required="required" value="{{ old('nm_produk', @$produk->nm_produk) }}">
            </div>
            <div class="form-group">
                <label for="inputName">Kategori Produk</label>
                <select name="kategori_produk" class="form-control">
                  <option value=""  >-- Kategori Produk --</option>
                  @foreach($kategoriproduk as $row)
                  <option value="{{ $row->jenis }}">{{$row->jenis}}</option>
                  @endforeach
                </select>
              </div>
               <div class="form-group">
                <label for="inputDescription">Deskripsi</label>
                <textarea name="deskripsi"  required="required" id="inputDescription" class="form-control" rows="4">
                  {{ old('deskripsi', @$produk->deskripsi) }}
                </textarea>
              </div>
              <div class="form-group">
                <label for="inputDescription">Harga</label>
              <div class="input-group flex-nowrap">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="addon-wrapping">Rp</span>
                </div>
                <input type="text" name="harga" name="number" required="required" id="inputDescription" class="form-control" rows="4" value="{{ old ('harga', @$produk->harga) }}">
              </div>
              </div>
            <div class="form-group">
                <label for="inputName">kuantitas</label>
                <input type="text" class="form-control" name="kuantitas" id="inputName" required="required" value="{{ old('kuantitas', @$produk->kuantitas) }}">
            </div>
            <div class="form-group">
               <label for="inputName">Weight</label>
               <div class="input-group flex-nowrap">
              <div class="input-group-prepend">
                  <span class="input-group-text" id="addon-wrapping">Gram</span>
                </div>
                <input type="text" class="form-control" name="weight" id="inputName" required="required" value="{{ old('weight', @$produk->weight) }}">
               </div>
              </div>
            <div class="form-group">
                <label for="inputName">Color</label>
                <input type="text" class="form-control" name="color" id="inputName"  value="{{ old('color', @$produk->color) }}">
            </div>
              <div class="form-group">
                <label for="inputName">Size</label>
                <input type="text" class="form-control" name="size" id="inputName"  value="{{ old('size', @$produk->size) }}">
            </div>
             <div class="form-group">
                <p class="textbox"><b>Gambar</b></p>      
                <input type="file" id="inputFile" name="gambar" required="required" placeholder="pilih gambar">
                <br/>
                <br/>
                <img id="image_upload" src="http://placehold.it/100x100" width="100px" height="100px" alt="img"/>
            </div>
            <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea" placeholder="Place some text here" name="model_gambar"  required="required" id="inputDescription"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('model_gambar',@$produk->model_gambar) }}
                </textarea>
              </div>
              <br/>
              <br/>
        <div class="row">
        <div class="col-12">
          <a href="/produk" type="button" class="btn btn-primary">Kembali</a>

          <input type="submit" value="Simpan Data" class="btn btn-primary" >
        </div>
      </div>
      </form>
             
            </div>
          </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

</div>
<!--footer-->
@include('Produk/footer')
<!--/footer-->
<!-- ./wrapper -->

<!-- jQuery -->


<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
<script>
  function readURL(input){
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e){
        $('#image_upload').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#inputFile").change(function () {
    readURL(this);
  });
</script>
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('AdminLte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('AdminLte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('AdminLte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('AdminLte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('AdminLte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('AdminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('AdminLte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('AdminLte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('AdminLte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
<!-- jQuery -->
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
<!-- jQuery -->
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
<!-- Summernote -->


<script src="{{asset('AdminLte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>




</body>
</html>