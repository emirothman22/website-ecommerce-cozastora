<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Produk</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLte/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.css')}}">                
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('AdminLte/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">

  
</head>
<body align="center" class="hold-transition sidebar-mini layout-fixed">
<style type="text/css">
  .tbl{
        margin: 20px auto;
        width: 1200px;
        padding: 10px;
        text-align: center;
        margin-left: 35px;
        vertical-align: center;
        line-height: 13px;
  }
  .table table-striped
  {
    border-radius: 20px;
    text-align: center;
  }
  
  .cari-data{
    font-family: monospace;
    font-size: 50sp;
    text-align: left;
  }
  h3{
    text-align: left;
    }
  #cari-data{
    width: 100px;
    position: relative;
  }
  #input-produk{
    width: 200px;
    margin-left: 850px;
  }
  .text{
    text-align: left;

  }
  .data{
    margin-right: 86%;
    margin-top: -1%;

  }
 
</style>

<div class="wrapper">
   <!--navabar-->
  @include('Produk/header')
  <!--/.navbar-->

<!--sidebar-->
@include('Produk/sidebar')
<!--/sidebar-->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Data Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Home</a></li>
              <li class="breadcrumb-item active">Produk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<center>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->


        <!--Membuat Tabel Produk-->
        

  <div class="tbl">
      <div class="data">
      <a href="/produk" class="btn btn-outline-success btn-sm">Data Produk</a>
      |
      <a href="/produk/trash"class="btn btn-outline-success btn-sm">Trash</a>
      </div>
    <br/>
    <br/>
    <a href="/produk/kembalikan_semua" class="btn btn-default btn-sm">Kembalikan Semua</a>
    |
    <a href="/produk/hapus_permanen_semua" class="btn btn-default btn-sm">Hapus Permanen Semua</a>
    <br/>
    <br/>
    @if(session('success'))
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert"> × </button> 
      {{ session('success') }}
    </div>
    @endif

    @if(session('error'))
    <div class="alert alert-error">
      {{ session('error') }}
    </div>
    @endif    
    <table class="table table-striped">
      <thead class="thead-dark"> 
      <tr class="table-danger" >
        <td align="center" scope="col" class="bg-primary"><h6> No </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Nama Produk </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6>Kategori Produk </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Deskripsi </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Harga </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Stok </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Gambar </h6></td>
        <td align="center" scope="col" class="bg-primary"><h6> Reviews </h6></td>
        <td align="center" scope="col" class="bg-danger"><h6> Aksi </h6></td>
     </tr>
      </thead>

    @foreach($produk as $rows)
  <tr>
    <center>
    <td class="bg-light">{{ isset($i) ? ++$i : $i =1}} </td>
    <td class="bg-light">{{ $rows->nm_produk }}</td>
    <td class="bg-light">{{ $rows->kategori_produk }}</td>
    <td class="bg-light">{{ $rows->deskripsi }}</td>
    <td class="bg-light">@currency($rows->harga)</td>
    <td class="bg-light">{{ $rows->kuantitas }}</td>
    <td class="bg-light"><img src="{{ url('uploadgambar') }}/{{ $rows->gambar }}" width="140px"></td>
    <td class="bg-light">{{ $rows->total_komentar }}</td>
    <td>
      <center>
      <a class="btn btn-info btn-sm" href="/produk/kembalikan/{{ $rows->id}}" style="margin-top:10%;"> Restore</a>

      <a class="btn btn-danger btn-sm" href="/produk/hapus_permanen/{{ $rows->id }}" style="margin-top:10%;">Hapus Permanen</a>
        
      </div>
      </td>


  </tr>
    @endforeach
</table>
{{ $produk->links() }}

<!--end table produk-->

          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

</div>
<!--footer-->
@include('Produk/footer')
<!--/footer-->

<!-- jQuery -->
<script type="text/javascript">
  
  var rupiah = document.getElementById('rupiah');
  rupiah.addEventListener('keyup', function(e){
    //tambahkan 'Rp.' pada saat form diketik
    //gunakan fungsi format rupiah() untuk mengubah angka yang diketik menjadi angka
    rupiah.value = formatRupiah(this.value, ' Rp. ');
  });

  /*Fungsi Format rupiah*/
  function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split     = number_string.split(','),
    sisa      = split[0].length % 3,
    rupiah    = split[0].substr(0, sisa),
    ribuan    = split[0].substr(sisa).match(/\d{3}/gi);

    //tambahkan titik jika yang diinput sedah menjadi angka ribuan
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] !=undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }

</script>
<script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('AdminLte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('AdminLte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('AdminLte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('AdminLte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('AdminLte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('AdminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('AdminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('AdminLte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('AdminLte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('AdminLte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('AdminLte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>
</body>
</html>



