 <!--Header -->
	<header>
		<style type="text/css">
			#image{
				margin-left: 20px;
			}
		</style>
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
					<div class="left-top-bar">
						Free shipping for standard order over $100
					</div>
							<div class="right-top-bar flex-w h-full">

						@guest
						<li>
						<a  href="{{ route('login') }}" class="flex-c-m trans-04 p-lr-25">
							Login
						</a>
						</li>
						 @if (Route::has('register'))
						<li> 
						<a href="{{ route('register') }}" class="flex-c-m trans-04 p-lr-25">
							Register
						</a>
						</li>
						 @endif
						  @else

						  <div class="menu-desktop">
							<ul class="main-menu">
							<li>
								<a href="#" style="color:#cccccc; font-size:85%">My Account</a>
								<ul class="sub-menu">
									<li><a href="/pengaturan" >---Pengaturan---</a></li>
									<li><a href="/pengaturan" >Pengaturan Account</a></li>
									<li><a href="/pengaturan/alamat">Pengaturan Alamat</a></li>
									
								</ul>
							</li>	
							</ul>
						  </div>

						 
                                    <a  id="navbarDropdown" class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
               
                                     @if (auth()->user()->provider != 'facebook' && auth()->user()->provider != 'google' )
									<img src="{{ url('uploadgambar') }}/{{ Auth::user()->gambar }}" class="rounded-circle" alt="User Image" height="40" width="50" id="image"  @if(auth()->user()->gambar === null) hidden @endif>	
									@else
									 <img alt="{{Auth::user()->name}}" src="{{Auth::user()->gambar}}" style="width: 50px; height: 40px; border-radius: 50%;"/>	
									@endif

                	        <a  id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                    {{ Auth::user()->name }} 

                                	</a>
                     

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                
                            
                        @endguest
					</div>
				</div>
			</div>

			<div class="wrap-menu-desktop">
				<nav class="limiter-menu-desktop container">
					
					<!-- Logo desktop -->		
					<a href="#" class="logo">
						<img src="{{asset('cozastore/images/icons/logo-01.png')}}" alt="IMG-LOGO">
					</a>

					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<li class="">
								<a href="/akunuser">Home</a>
								
							</li>

							<li class="">

								<a href="/product">Shop</a>
							</li>
							
							@if (Auth::check())
							<li class="label1" data-label1="hot">
								<a href="#">Features</a>
								<ul class="sub-menu">
									<li><a href="/order/cart" >Product Cart</a></li>
									<li><a href="/order/wishlist">Product Wishlist</a></li>
								</ul>
							</li>	
							@else
							<li class="label1" data-label1="hot">
								<a href="/order">Features</a>
							</li>
							@endif

							<li>
								<a href="/news">News</a>
							</li>

							<li>
								<a href="/about">About</a>
							</li>

							<li>
								<a href="/contact">Contact</a>
							</li>
						</ul>
					</div>		

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m">
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">
							<i class="zmdi zmdi-search"></i>
						</div>

						@if (Auth::check())
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="{{$totalkeranjang}}" data-toggle="tooltip" data-placement="right" title="Keranjang Belanja">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>

						<a href="/order/wishlist" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="{{$totalsuka}}" data-toggle="tooltip" data-placement="right" title="like product">
							<i class="zmdi zmdi-favorite-outline"></i>
						</a>	
						@else
							
						@endif
						
					</div>
				</nav>
			</div>	
		</div>

		<!-- Header Mobile -->
		<div class="wrap-header-mobile">
			<!-- Logo moblie -->		
			<div class="logo-mobile">
				<a href="index.html"><img src="{{asset('cozastore/images/icons/logo-01.png')}}" alt="IMG-LOGO"></a>
			</div>

			<!-- Icon header -->
			<div class="wrap-icon-header flex-w flex-r-m m-r-15">
				<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">
					<i class="zmdi zmdi-search"></i>
				</div>

				<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="2" >
					<i class="zmdi zmdi-shopping-cart"></i>
				</div>

				<a href="#" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti" data-notify="0" >
					<i class="zmdi zmdi-favorite-outline"></i>
				</a>
			</div>

			<!-- Button show menu -->
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>


		<!-- Menu Mobile -->
		<div class="menu-mobile">
			<ul class="topbar-mobile">
				<li>
					<div class="left-top-bar">
						Free shipping for standard order over $100
					</div>
				</li>

				<li>
					<div class="right-top-bar flex-w h-full">
						<a href="#" class="flex-c-m p-lr-10 trans-04">
							Help & FAQs
						</a>

						<a href="#" class="flex-c-m p-lr-10 trans-04">
							My Account
						</a>

						<a href="#" class="flex-c-m p-lr-10 trans-04">
							EN
						</a>

						<a href="#" class="flex-c-m p-lr-10 trans-04">
							USD
						</a>
					</div>
				</li>
			</ul>

			<ul class="main-menu-m">
				<li>
					<a href="index.html">Home</a>
					<ul class="sub-menu-m">
						<li><a href="index.html">Homepage 1</a></li>
						<li><a href="home-02.html">Homepage 2</a></li>
						<li><a href="home-03.html">Homepage 3</a></li>
					</ul>
					<span class="arrow-main-menu-m">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</span>
				</li>

				<li>
					<a href="product.html">Shop</a>
				</li>

				<li>
					<a href="shoping-cart.html" class="label1 rs1" data-label1="hot">Features</a>
				</li>

				<li>
					<a href="blog.html">Blog</a>
				</li>

				<li>
					<a href="about.html">About</a>
				</li>

				<li>
					<a href="contact.html">Contact</a>
				</li>
			</ul>
		</div>

		<!-- Modal Search -->
		<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
			<div class="container-search-header">
				<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
					<img src="{{asset('cozastore/images/icons/icon-close2.png')}}" alt="CLOSE">
				</button>

				<form class="wrap-search-header flex-w p-l-15">
					<button class="flex-c-m trans-04">
						<i class="zmdi zmdi-search"></i>
					</button>
					<input class="plh3" type="text" name="search" placeholder="Search...">
				</form>
			</div>
		</div>
				<script>
	
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
 
</script>
	</header>
	<!--END HEADER