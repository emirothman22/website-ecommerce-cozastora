<!DOCTYPE html>
<html>
<head>
	<title>Login Form Desaign</title>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<style type="text/css">
	body{
	margin: 0;
	padding: 0;
	font-family: sans-serif;
	letter-spacing: 1px;
	background: url('cozastore/images/bg-02.jpg') no-repeat;
	background-size: cover;
	 
}
.login{
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%,-50%);
	width: 340px;
	height: 400px;
	padding: 80px 40px;
	box-sizing: border-box;
	background: rgba(0,0,0,.8);
}
.login:before{
	content: '';
	position: absolute;
	top: 0;
	left: 0;
	width: 50%;
	height: 100%;
	background: rgba(255,255,255,.1);
	pointer-events: none;
}
.man{
	width: 100px;
	height: 100px;
	border-radius: 50%;
	position: absolute;
	top: calc(-100px/2);
	left: calc(50% - 50px);
}
h2{
	margin: 0;
	padding: 0 0 20px;
	color: #efed40;
	text-align: center;
}
.login p{
	margin: 0;
	padding: 0;
	font-weight: bold;
	color: #fff;
}

.login input{
	width: 100%;
	margin-bottom: 20px;
}
.login input[type="text"],
.login input[type="Password"]
{
	border: none;
	border-bottom: 1px solid #fff;
	background: transparent;
	outline: none;
	color: #fff;
	font-size: 16px;
	height: 40px;
}
.login input[type="submit"]
{
	border: none;
	outline: none;
	color: #262626;
	background: #fbc531;
	cursor: pointer;
	border-radius: 20px;
	height: 40px;
	font-size: 16px;
}
.login input[type="submit"]:hover
{
	background: #efed40;
	color: #262626;
}
.login a{
	color: #fff;
	font-size: 14px;
	font-weight: bold;
	text-decoration: none;
}
.login a:hover{
	color: #efed40;
}





</style>
<body>
	<div class="login">
		<img src="cozastore/images/images.png" class="man">
		<h2>Login</h2>
		<form class="form-login" method="POST" action="{{route('register') }}">
			{{ csrf_field() }}
			<p class="textbox">Name</p>
			<input type="" name="">
			<p class="textbox">Email Address</p>
			<input type="text" name="" placeholder="Enter Email">

			<p class="textbox">Password</p>
			<input type="Password" name="" placeholder="......">

			<p class="textbox">Confirm Passwords</p>
			<input type="Password" name="" placeholder="......">
			<input type="submit" name="" value="Sign in">

			<a href="#">Forget Password</a>
		</form>
	</div>


</body>
</html>