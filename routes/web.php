<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Halaman Admin//
Route::get('/admin','AdminController@index');
Route::get('/banner','BannerController@index');

//fitur user 
Route::get('/user','UserController@index');
Route::get('/user/delete/{id}','UserController@delete');
Route::get('/user/cari','UserController@cari');
//

//transaksi
Route::get('/transaksi','TransaksiController@index');
Route::get('/transaksi/cari','TransaksiController@cari');

//Pesanan
Route::get('/pesanan','PesananController@pesanan');
Route::get('/pesanan/cari','PesananController@cari');
//function form inbox
Route::get('/inbox','InboxController@index');
Route::get('/inbox/read/{id}','InboxController@read');
Route::get('/compose','ComposeController@index');
Route::get('/inbox/hapus/{id}','InboxController@hapus');
Route::get('/produk','ProdukController@index');
Route::get('/about_admin','AboutController@index');
Route::get('/kategori','KategoriController@index');


//CRUD KATEGORI PRODUK//
Route::get('/kategori','KategoriController@index');
Route::get('/kategori/tambah','KategoriController@tambah');
Route::post('/kategori','KategoriController@store');
Route::get('/kategori/edit/{id}','KategoriController@edit');
Route::patch('/kategori/{id}','KategoriController@update');
Route::get('/kategori/hapus/{id}','KategoriController@hapus');
Route::get('/kategori/cari','KategoriController@cari');

//CRUD KATEGORI BERITA//
Route::get('/kategoriberita','KategoriBeritaController@index');
Route::get('/kategoriberita/tambah','KategoriBeritaController@tambah');
Route::post('/kategoriberita','KategoriBeritaController@proses');
Route::get('/kategoriberita/edit/{id}','KategoriBeritaController@edit');
Route::patch('/kategoriberita/{id}','KategoriBeritaController@update');
Route::get('/kategoriberita/hapus/{id}','KategoriBeritaController@hapus');
Route::get('/kategoriberita/cari','KategoriController@cari');


//CRUD PRODUK//
Route::get('/produk','ProdukController@index');
Route::get('/produk/eproduk','ProdukController@lihatProduk');
Route::get('/produk/eproduk/{id}','ProdukController@detailProduk');
Route::get('/produk/tambah','ProdukController@tambah');
Route::post('/produk','ProdukController@store');
Route::get('/produk/hapus/{id}','ProdukController@hapus');
Route::get('/produk/trash','ProdukController@trash');
Route::get('/produk/kembalikan/{id}','ProdukController@kembalikan');
Route::get('/produk/kembalikan_semua','ProdukController@kembalikan_semua');
Route::get('/produk/hapus_permanen/{id}','ProdukController@hapus_permanen');
Route::get('/produk/hapus_permanen_semua','ProdukController@allDelete');
Route::get('/produk/edit/{id}','ProdukController@edit');
Route::patch('/produk/{id}','ProdukController@update');
Route::get('/produk/cari','ProdukController@cari');


//CRUD BERITA//
Route::get('/berita','BeritaController@index');
Route::get('/viewberita/sampah','BeritaController@sampah');
Route::get('/viewberita','BeritaController@viewBerita');
Route::get('/viewberita/tambah','BeritaController@tambah');
Route::get('/viewberita/detailberita/{id}','BeritaController@detailBerita');
Route::post('/viewberita','BeritaController@store');
Route::get('/viewberita/hapus/{id}','BeritaController@hapus');
Route::get('/viewberita/kembalikan/{id}','BeritaController@restore');
Route::get('/viewberita/hapus_permanen/{id}','BeritaController@deletePermanen');
Route::get('/viewberita/kembalikan_semua','BeritaController@returnAll');
Route::get('/viewberita/hapus_permanen_semua','BeritaController@deleteAll');
Route::get('/viewberita/edit/{id}','BeritaController@edit');
Route::patch('/viewberita/{id}','BeritaController@update');
Route::get('/berita/cari','BeritaController@cari');

//CRUD BANNER//
Route::get('/banner','BannerController@index');
Route::get('/banner/tambah','BannerController@tambah');
Route::post('/banner/store','BannerController@store');
Route::get('/banner/hapus/{id}','BannerController@hapus');
Route::get('/banner/cari','BannerController@cari');

//CartAdmin Layout
Route::get('/cart','CartAdminController@cartAdmin');
Route::get('/cart/cari','CartAdminController@cari');

// Notif For Admin
Route::get('/notifikasi','NotifikasiController@index');
Route::get('/notifikasi/delete/{id}','NotifikasiController@deleteNotifikasi');
Route::get('/notifikasi/detailnotifikasi/{id}','NotifikasiController@detailNotifikasi');


//End Layout Admin// 	 	


//Halaman User//
Route::get('/akunuser','UserLayoutController@index');
Route::get('/akunuser/productdetail{id}','UserLayoutController@index2');
Route::get('/akunuser/{jenis}','UserLayoutController@filterdata');


//Halaman Produk//
Route::get('/product','TokoController@index');
Route::get('/product/productdetail/{id}','TokoController@index2');
Route::post('/product/productdetail/{id}','KomentarController@komentarProduk');
Route::patch('/product/productdetail/{id}/delete','KomentarController@deleteProduk');
Route::get('/product/{jenis}','TokoController@filterdata');
Route::get('/product/cari','TokoController@cari');
Route::get('/order','DetailOrderController@index');
Route::get('/about','AboutUserController@index');


//proses backend contack//
Route::get('/contact','ContactUserController@index');
Route::get('/contact','ContactUserController@createKomen');
Route::post('/contact','ContactUserController@kirimKomentar');

//Halaman Berita//
Route::get('/news','NewsUserController@index');
Route::get('/news/cari','NewsUserController@cari');
Route::get('/news/detailnews/{id}','NewsUserController@index2');
Route::post('/news/detailnews/{id}','KomentarController@komentarBerita');
// Route::get('/product/productdetail/{id}','NewsUserController@index3');
Route::get('/news/{jenis}','NewsUserController@filterdata');
Route::get('/news/{bulan}','NewsUserController@filterdata3');

//Halaman Login and Register//
Route::get('/register','AuthController@register');
Route::get('/login','AuthController@login');
//setting akun
Route::get('/pengaturan','AuthController@pengaturan');
Route::put('/akunuser/{id}','AuthController@update');
// Route::get('/akunuser/pengaturan{id}','AuthController@updateprofile');
//setting alamat
Route::get('/pengaturan/alamat','AuthController@alamat');
Route::put('/pengaturan/alamat/{id}','AuthController@updateAlamat');

//end Halaman Pages

//Wishlist
Route::get('/order/wishlist','WishlistController@wishlist');
Route::post('/wishlist/{id}','WishlistController@addWishlist')->middleware('auth');
Route::delete('/wishlist/{id}','WishlistController@remove');

//keranjang
Route::get('/order/cart','DetailOrderController@index');
Route::post('/order/{id}/add','DetailOrderController@addKeranjang');
Route::delete('/order/{id}','DetailOrderController@remove');
Route::PATCH('/order/{id}','DetailOrderController@updateKeranjang');
Route::get('/order/{id}/add/session','DetailOrderController@addSession');
Route::delete('order/remove/session', 'DetailOrderController@removeSession');
Route::PATCH('order/update/session', 'DetailOrderController@updateSession');

//chechout
Route::get('/order/checkout','CheckoutController@checkout')->middleware('auth');
Route::post('/order/checkout', 'CheckoutController@submit');
Route::get('/province/{id}/cities','AuthController@getCities');
Route::post('/order/process-checkout','DetailOrderController@submit')->middleware('auth');

//Transaction
Route::get('/order/transaction','TransaksiController@transaksi');
Route::post('/order/transaction','TransaksiController@pesan');
Route::get('/order/shipping/{id}/pilih','TransaksiController@pilihOngkir');


Auth::routes();
Route::get('/akunuser', 'UserLayoutController@index')->name('akunuser');
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
Route::get('/login/{provider}', 'SocialAuthGoogleController@redirectToProvider');
Route::get('/login/{provider}/callback', 'SocialAuthGoogleController@callback');

//Login and register Admin
Route::get('/loginadmin','AuthenticController@login'); 
Route::get('/registeradmin','AuthenticController@register'); 
 

